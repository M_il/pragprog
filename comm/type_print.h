#ifndef TYPE_PRINT_H
#define TYPE_PRINT_H

#import <gsl/gsl_vector.h>
#import <gsl/gsl_matrix.h>

void printm(gsl_matrix* A);
void printv(gsl_vector* v);

#endif
