#import "type_print.h"
#define FMT "%7.3f"

void printm(gsl_matrix* A){
	for(int r=0;r<A->size1;r++){
		for(int c=0;c<A->size2;c++)
      printf(FMT,gsl_matrix_get(A,r,c));
		printf("\n");
  }
}

void printv(gsl_vector* v){
  printf("[");
	for(int r=0;r<v->size;r++){
    printf(FMT,gsl_vector_get(v,r));
		printf(",");
  }
  printf("]\n");
}
