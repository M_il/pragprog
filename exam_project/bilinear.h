#ifndef BILINEAR_H
#define BILINEAR_H

#include <math.h>
#include <assert.h>
#include <givens.h>
#include <gsl/gsl_matrix.h>

double bilinear(
  int nx, int ny,       // Number of grid points in x- and y-direction
  double* x, double* y, // (x,y) coordinates for rectlinear grid
  double** F,           // Function evaluted at each grid point
  double px, double py);// Interpolat

#endif
