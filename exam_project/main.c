#include "bilinear.h"
#include <stdlib.h>
#include <stdio.h>

#define RND ((double)rand()/RAND_MAX)

double F1(double x, double y) {
  return 15*(x-5)*(x-5) + 5*(y-5);
}

double F2(double x, double y) {
  return sin(x/2)+cos(y);
}

// Just placed here to look more tidy
double** matrix_alloc(int n, int m) {
  double **p = malloc(sizeof(double*) * n);
  for (int i = 0; i < n; i++)
    p[i] = malloc(sizeof(double) * m);
  return p;
}

// Evaluate a function on a grid of points
void evaluate_on_grid(double** F, double f(double,double), int nx, double x[], int ny, double y[]) {
  for (int ix = 0; ix < nx; ix++) {
    for (int iy = 0; iy < ny; iy++) {
      F[ix][iy] = f(x[ix], y[iy]);
    }
  }
}

// Print the bi-linear iterpolation of a function on a rectlinear grid
void print_bilinear_interp(int nx, int ny, double x[], double y[], double** F, int n_interp) {
  for (double px = x[0]; px < x[nx-1]; px+=(x[nx-1]-x[0])/n_interp) {
    for (double py = y[0]; py < y[ny-1]; py+=(y[ny-1]-y[0])/n_interp) {
      double F_inter = bilinear(nx,ny,x,y,F,px,py);
      printf("%g %g %g\n", px, py, F_inter);
    }
  }
}

int main(int argc, char const *argv[]) {
  int nx = 10, ny = 5;
  double x[nx],y[ny]; // VLA as of c99
  double **F = matrix_alloc(nx,ny);

  // Generate random rectilinear grid
  x[0] = RND; y[0] = RND;
  for (int ix = 1; ix < nx; ix++) {
    x[ix] = x[ix-1] + 3*RND;
  }
  for (int iy = 1; iy < ny; iy++) {
    y[iy] = y[iy-1] + 4*RND;
  }

  // Print rectilinear grid
  printf("# rectilinear grid coordinates\n");
  for (int ix = 0; ix < nx; ix++)
    for (int iy = 0; iy < ny; iy++)
      printf("%g %g 0\n", x[ix], y[iy]);

  printf("\n\n");

  // Generate interpolated points
  printf("# Iterpolated function 1\n");
  // Evaluate the funcion on each point of the grid
  evaluate_on_grid(F,F1,nx,x,ny,y);
  print_bilinear_interp(nx,ny,x,y,F,20);

  printf("\n\n");

  printf("# Iterpolated function 2\n");
  evaluate_on_grid(F,F2,nx,x,ny,y);
  print_bilinear_interp(nx,ny,x,y,F,20);

  // Free the allocated space,
  // although not requered when main is returning
  for (int i = 0; i < nx; i++) free(F[i]);
  free(F);

  return 0;
}
