#import "bilinear.h"

// Binary search for where z appears in x of length n
int binary_search(double z, int n, double* x) {
  int i=0, j=n-1, m;
  while (j-i>1) {
    m=(i+j)/2;
    if (z>x[m])
      i=m;
    else
      j=m;
  }
  return i;
}

// Determine a linear iterplation of a tabulated function on a rectlinear grid
double bilinear(
  int nx, int ny,       // Number of grid points in x- and y-direction
  double* x, double* y, // (x,y) coordinates for rectlinear grid
  double** F,           // Interpolant evaluted at each grid point
  double px, double py) // Point to interplate p = (px,py)
{
  // Sanity check that rectlinear grid is of valid size, ...
  assert(nx > 1 && ny > 1);
  // and that p lie within the grid
  assert(x[0] <= px && px <= x[nx-1] && y[0] <= py && py <= y[ny-1]);

  // Binary search for where p lies with respect to tabulated rectlinear grid
  int ix = binary_search(px, nx, x);
  int iy = binary_search(py, ny, y);

  /*
    Calculate the coefficients for the bi-linear interplation
    of the form B(x,y) = a + b*x + c*y + d*x*y
    This may be solved using the equations of following wiki article:
    https://en.wikipedia.org/wiki/Bilinear_interpolation#Alternative_algorithm
    Note: This may also be calculated as the solution to a linear system
          of four equation, but as closed from exact epressions are available
          these are not used. That is, the equations below are simply found
          by inverting the matrix of the linear system (which may also have been
          done using one of the previous exersices, e.g. qr-decomposition).
  */
  double x1 = x[ix], x2 = x[ix+1], y1 = y[iy], y2 = y[iy+1];
  double F11 = F[ix][iy], F21 = F[ix+1][iy], F12 = F[ix][iy+1], F22 = F[ix+1][iy+1];
  double a,b,c,d;
  a = x2*(y1*F12-y2*F11) - x1*(y1*F22-y2*F21),
  b = y2*(F11-F21) + y1*(F22-F12),
  c = x2*(F11-F12) - x1*(F21-F22),
  d = F21+F12-F22-F11;

  a/=(x1-x2)*(y2-y1); // All divided by the same factor
  b/=(x1-x2)*(y2-y1);
  c/=(x1-x2)*(y2-y1);
  d/=(x1-x2)*(y2-y1);

  // Return the spline function
  return a + b*px + c*py + d*px*py;
}
