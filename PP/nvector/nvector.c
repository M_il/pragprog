#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "../epsilon/equal.h"
#include "nvector.h"
#define ITTR_NVECTOR(v,i) for (int i = 0; i < v->size; i++)

int double_equal(double a, double b) {
  double TAU = 1e-6, EPS = 1e-6;
  return equal(a, b, TAU, EPS);
}

nvector* nvector_alloc(int n) {
  assert(0 < n);
  nvector* v = malloc(sizeof(nvector));
  v->size = n;
  v->data = malloc(n*sizeof(double));
  if(v == NULL)
    fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

void nvector_free(nvector* v) {
  free(v->data);
  free(v);
}

void nvector_set(nvector* v, int i, double value) {
  assert(0 <= i && i < v->size);
  v->data[i] = value;
}

double nvector_get(nvector* v, int i) {
  assert(0 <= i && i < (*v).size);
  return v->data[i];
}

double nvector_dot_product(nvector* u, nvector* v) {
  assert(u->size == u->size);
  double sum = 0;
  for (int i = 0; i < (*u).size; i++) {
    sum += u->data[i] * v->data[i];
  }
  return sum;
}

void nvector_print(char* s, nvector* v) {
  printf("%s(", s);
  for (int i = 0; i < v->size - 1; i++) {
    printf("%g, ", v->data[i]);
  }
  printf("%g", v->data[v->size - 1]);
  printf(")\n");
}

void nvector_set_zero(nvector* v) {
  for (int i = 0; i < v->size; i++) {
    v->data[i] = 0;
  }
}

int nvector_equal(nvector* a, nvector* b) {
  assert(a->size == b->size);
  ITTR_NVECTOR(a,i)
    if (a->data[i] != b->data[i])
      return 0;
  return 1;
}

void nvector_add(nvector* a, nvector* b) {
  assert(a->size == b->size);
  ITTR_NVECTOR(a,i)
    a->data[i] += b->data[i];
}

void nvector_sub(nvector* a, nvector* b) {
  assert(a->size == b->size);
  ITTR_NVECTOR(a,i)
    a->data[i] -= b->data[i];
}

void nvector_scale(nvector* a, double x) {
  ITTR_NVECTOR(a,i)
    a->data[i] *= x;
}
