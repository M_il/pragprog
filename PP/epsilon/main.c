#include <limits.h>
#include <stdio.h>
#include <float.h>
#include "equal.h"

void name_digit(int i);

void newLine(void) {
  printf("\n");
}

int main(void) {

  // Maximum integer using while, for, and do-while loops
  int i=1; while(i+1>i) {i++;}
  int j = 0;
  for(; j+1>j; j++){}
  int k = 0;
  do {k++;} while (k+1>k);
  printf("My max int:\n");
  printf("Using while:    %i\n",i);
  printf("Using for:      %i\n",j);
  printf("Using do-while: %i\n",k);
  printf("Actual INT_MAX: %i\n", INT_MAX);

  // Minimum integer using while, for, and do-while loops
  i = 1; while(i-1<i) {i--;}
  j = 1;
  for(; j-1<j; j--){}
  k = 1;
  do {k--;} while (k-1<k);
  newLine();
  printf("My min int:\n");
  printf("Using while:    %i\n",i);
  printf("Using for:      %i\n",j);
  printf("Using do-while: %i\n",k);
  printf("Actual INT_MIN: %i\n", INT_MIN);

  // Machine epsilon
  float x; double y; long double z;
  x=1; while(1+x!=1){x/=2;} x*=2;
  y=1; while(1+y!=1){y/=2;} y*=2;
  z=1; while(1+z!=1){z/=2;} z*=2;
  newLine();
  printf("My machine epsilon using while:\n");
  printf("Float:               %g\n", x);
  printf("Double:              %g\n", y);
  printf("Long double:         %Lg\n", z);

  x=1; for(;1+x!=1;){x/=2;} x*=2;
  y=1; for(;1+y!=1;){y/=2;} y*=2;
  z=1; for(;1+z!=1;){z/=2;} z*=2;
  newLine();
  printf("My machine epsilon using for:\n");
  printf("Float:               %g\n", x);
  printf("Double:              %g\n", y);
  printf("Long double:         %Lg\n", z);

  x=1; do {x/=2;} while(1+x!=1); x*=2;
  y=1; do {y/=2;} while(1+y!=1); y*=2;
  z=1; do {z/=2;} while(1+z!=1); z*=2;
  newLine();
  printf("My machine epsilon using do-while:\n");
  printf("Float:               %g\n", x);
  printf("Double:              %g\n", y);
  printf("Long double:         %Lg\n", z);

  newLine();
  printf("Actual FLT_EPSILON:  %g\n", FLT_EPSILON);
  printf("Actual DBL_EPSILON:  %g\n", DBL_EPSILON);
  printf("Actual LDBL_EPSILON: %Lg\n", LDBL_EPSILON);

  newLine();
  // Float sum
  printf("Float sum:\n");
  int max=INT_MAX/2;
  float sum_up_float = 0;
  float sum_down_float = 0;

  for (int i=1; i<max; i++) {
    sum_up_float += 1.0f/i;
  }
  for (int i=max; i>=1; i--) {
   sum_down_float += 1.0f/i;
  }

  printf("Summing from large to small: %g\n", sum_up_float);
  printf("Summing from small to large: %g\n", sum_down_float);
  printf("That's a difference of: %g\n", sum_down_float - sum_up_float);
  printf("Because larger floating point numbers are less precise, i.e. not able\n"
         "to represet even smaller numbers given a fixed exponent, adding a small\n"
         "number to a large one may not change the result from the larger one.\n"
         "Thus, summing many small numbers a first lets the small ones have\n"
         "change at accumulating to something bigger in the end, while adding the\n"
         "larger numbers at first fixes the exponent such that smaller numbers\n"
         "do not affect the sum.\n");
  printf("In (mathematical) reality the sum does not converge for max -> infinity.\n"
         "But for finite max-value the sum has the value of the max'th 'Harmonic number'\n"
         "As for computational cenvergance, the FLT_EPSILON sets the limit for\n"
         "how many terms that may be summed.\n");

  // Double sum
  newLine();
  printf("Now using double:\n");

  double sum_up_double = 0;
  double sum_down_double = 0;

  for (int i=1; i<max; i++) {
    sum_up_double += 1.0f/i;
  }
  for (int i=max; i>=1; i--) {
   sum_down_double += 1.0f/i;
  }

  printf("Summing from large to small: %g\n", sum_up_double);
  printf("Summing from small to large: %g\n", sum_down_double);
  printf("That's a difference of: %g\n", sum_down_double - sum_up_double);
  printf("Thus, as a double is more precise, the results are closer.\n");

  newLine();
  printf("equal(a=1.2, b=1.3, tau=0.02, epsilon=0.7) = %i\n", equal(1.2, 1.3, 0.02, 0.7));
  printf("equal(a=1.2, b=1.3, tau=0.2, epsilon=0.07) = %i\n", equal(1.2, 1.3, 0.2, 0.07));
  printf("equal(a=1.2, b=1.3, tau=0.2, epsilon=0.7)  = %i\n", equal(1.2, 1.3, 0.2, 0.7));

  newLine();
  for (int i = 0; i < 9; i++) {
    printf("name_digit(%i) = ", i);
    name_digit(i);
    newLine();
  }

  return 0;
}
