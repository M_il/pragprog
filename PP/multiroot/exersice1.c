#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>

// Using example from [https://www.gnu.org/software/gsl/doc/html/multiroots.html]

int rosenbrock_grad(const gsl_vector* xy, void* params, gsl_vector* f) {
  const double x = gsl_vector_get(xy, 0);
  const double y = gsl_vector_get(xy, 1);

  // 2(200x³-200xy+x-1), 200(y-x²)
  const double f0 = 2 * (200 * x*x*x - 200*x*y + x - 1);
  const double f1 = 200 * (y - x*x);

  gsl_vector_set(f, 0, f0);
  gsl_vector_set(f, 1, f1);

  return GSL_SUCCESS;
}

void print_state(int iter, gsl_multiroot_fsolver * s) {
  printf("iter = %3u, (x,y) = (%.3f, %.3f) "
         "grad f(x,y) = (%.3e, %.3e)\n",
         iter,
         gsl_vector_get(s->x, 0),
         gsl_vector_get(s->x, 1),
         gsl_vector_get(s->f, 0),
         gsl_vector_get(s->f, 1));
}

int main(int argc, char const *argv[]) {
  printf(
    "Finding extermum of Rosebnbrock function:\n"
    "f(x,y) = (1-x)² + 100(y-x²)².\n"
    "Its gradient is:\n"
    "∇f(x,y) = {-2(1-x) + -400x(y-x²), 200(y-x²)}\n"
    "        = {2(200x³-200xy+x-1), 200(y-x²)}.\n\n"
  );

  gsl_multiroot_fsolver *s;

  int status;
  int iter = 0;

  const size_t n = 2;
  gsl_multiroot_function f = {&rosenbrock_grad, n, NULL};

  double xy_init[2] = {2, 3};
  gsl_vector* xy = gsl_vector_alloc(n);

  gsl_vector_set(xy, 0, xy_init[0]);
  gsl_vector_set(xy, 1, xy_init[1]);

  s = gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_hybrids, 2);
  gsl_multiroot_fsolver_set(s, &f, xy);

  printf("The initial state is:\n");
  print_state(iter, s);

  printf("And for the irrerations:\n");
  do {
    iter++;
    status = gsl_multiroot_fsolver_iterate(s);

    print_state(iter, s);

    if (status) // If it's stuck then break
      break;

    status = gsl_multiroot_test_residual(s->f, 1e-10);
  } while (status == GSL_CONTINUE && iter < 1000);

  printf("status = %s\n", gsl_strerror(status));

  double x = gsl_vector_get(s->x, 0);
  double y = gsl_vector_get(s->x, 1);
  printf("Converged at f(x,y) = %g", (1-x)*(1-x) + 100*(y-x*x)*(y-x*x));

  gsl_multiroot_fsolver_free(s);
  gsl_vector_free(xy);

  return 0;
}
