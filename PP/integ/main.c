#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

double f1(double x, void* params) {
  return log(x) / sqrt(x);
}

double norm(double x, void* params) {
  double alpha = *(double*)params;
  return 2*exp(-alpha * x*x);
}

double H_so(double x, void* params) {
  double alpha = *(double*)params;
  return 2*(-alpha*alpha*x*x/2 + alpha/2 + x*x/2) * exp(-alpha * x*x);
}

double E_alpha(double alpha, gsl_integration_workspace* w) {
  gsl_function F;
  double result_norm, error_norm;
  double result_H_so, error_H_so;

  F.function = &norm;
  F.params = &alpha;

  double epsabs = 0, epsrel = 1e-7;

  // Using the symmetry of the functions
  gsl_integration_qagiu(&F, 0, epsabs, epsrel, w->limit,
                           w, &result_norm, &error_norm);

  F.function = &H_so;
  gsl_integration_qagiu(&F, 0, epsabs, epsrel, w->limit,
                           w, &result_H_so, &error_H_so);

  return result_H_so/result_norm;
}

int main(int argc, char** argv){
  int limit = 1000;
  gsl_integration_workspace* w = gsl_integration_workspace_alloc(limit);

  double result, error;

  gsl_function F;
  F.function = &f1;
  F.params = NULL;

  // Exersice 1
  double a = 0, b = 1;
  double epsabs = 0, apsrel = 1e-7;
  double exact = -4;

  gsl_integration_qags(&F, a, b, epsabs, apsrel, limit,
                        w, &result, &error);

  printf("Result of int. from 0 to 1 of log(x)/sqrt(x):\n");
  printf("result          = %g\n", result);
  printf("exact           = %g\n", exact);
  printf("estimated error = %g\n", error);
  printf("actual error    = %g\n", exact - result);
  printf("intervals       = %zu\n", w->size);

  // Exersice 2
  // Using './main exersice2.data', write to a file named exersice2.data
  if (argc != 2) {return 0;}
  FILE* fd = fopen(argv[1], "w");

  for (double alpha = 0.2; alpha < 2; alpha+=0.01) {
    fprintf(fd, "%g\t%g\n", alpha, E_alpha(alpha, w));
  }

  fclose(fd);

  gsl_integration_workspace_free(w);

  return 0;
}
