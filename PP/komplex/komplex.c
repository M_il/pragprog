#include <stdio.h>
#include <math.h>
#include"komplex.h"

void komplex_print(char *s, komplex a) {
	printf ("%s (%g,%g)\n", s, a.re, a.im);
}

komplex komplex_new(double x, double y) {
	komplex z = { x, y };
	return z;
}

void komplex_set(komplex* z, double x, double y) {
	(*z).re = x;
	(*z).im = y;
}

komplex komplex_add(komplex a, komplex b) {
	komplex result = { a.re + b.re , a.im + b.im };
	return result;
}

komplex komplex_sub(komplex a, komplex b) {
  return (komplex){a.re - b.re, a.im - b.im};
}

int komplex_equal(komplex a, komplex b) {
  return (a.re == b.re) && (a.im == b.im);
}

// (a.re + i(a.im))*(b.re + i(b.im))
// = a.re^2  - a.im*b.im + i(a.re*b.im + a.im*b.im)
komplex komplex_mul(komplex a, komplex b) {
  komplex result = {
    a.re*b.re - a.im*b.im,
    a.re*b.im + a.im*b.re
  };
  return result;
}

// (a.re + i(a.im))/(b.re + i(b.im)) =
// (a.re + i(a.im))*(b.re + i(b.im)) / (b.re + i(b.im))^2
komplex komplex_div(komplex a, komplex b) {
  komplex ab = komplex_mul(a,b);
  double bb  = komplex_mul(b,b).re;
  return (komplex){ab.re/bb,ab.im/bb};
}

komplex komplex_conjugate(komplex z) {
  return (komplex){z.re, -z.im};
}

komplex komplex_abs(komplex z) {
  return (komplex){sqrt(pow(z.re, 2) + pow(z.im, 2)),0};
}

// exp(z.re + i(z.im)) = exp(z.re)*(cos(z.im) + i(sin(z.im)))
komplex komplex_exp(komplex z) {
  double expRe = exp(z.re);
  return (komplex){expRe*cos(z.im), expRe*sin(z.im)};
}

// sin z = 1/2 * i * (exp(-iz) - exp(iz))
komplex komplex_sin(komplex z) {
  komplex halfI = {0, 1.0/2};
  komplex I = {0, 1.0};
  komplex minusI = {0, -1.0};
  komplex expMinusIZ = komplex_exp(komplex_mul(minusI, z));
  komplex expIZ = komplex_exp(komplex_mul(I, z));
  return komplex_mul(halfI, komplex_sub(expMinusIZ, expIZ));
}

// cos z = 1/2 * (exp(-z) + exp(z))
komplex komplex_cos(komplex z) {
  komplex half = {1.0/2, 0};
  komplex I = {0, 1.0};
  komplex minusI = {0, -1.0};
  komplex expMinusZ = komplex_exp(komplex_mul(minusI, z));
  komplex expZ = komplex_exp(komplex_mul(I, z));
  return komplex_mul(half, komplex_add(expZ, expMinusZ));
}

// z ~ r: radi, theta: argument
komplex komplex_sqrt(komplex z) {
  double r = komplex_abs(z).re;
  double theta = atan2(z.im, z.re); // (y,x)

  komplex sqrtR = {sqrt(r),0};
  komplex expThetaHalf = komplex_exp((komplex){0,theta/2});

  return komplex_mul(sqrtR, expThetaHalf);
}
