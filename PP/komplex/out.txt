testing komplex_add...
a = (1,2)
b = (3,4)
a+b should   = (4,6)
a+b actually = (4,6)
test 'add' passed :)
a - b (-2,-2)
a * b = (-5,10)
a / b = (0.714286,-1.42857)
ā = (1,-2)
|a| = (2.23607,0)
exp(a) = (-1.1312,2.47173)
sin(a) = (3.16578,1.9596)
cos(a) = (2.03272,-3.0519)
sqrt(a) = (1.27202,0.786151)
