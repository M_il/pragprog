#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multimin.h>

double rosenbrock(const gsl_vector *v, void *params) {
  double x = gsl_vector_get(v, 0);
  double y = gsl_vector_get(v, 1);
  return (1-x)*(1-x) + 100*(y-x*x)*(y-x*x);
}

int main(void) {
  const gsl_multimin_fminimizer_type *T =
    gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer *s = NULL;
  gsl_vector *ss, *xy;
  gsl_multimin_function minex_func;

  int iter = 0;
  int status;
  double size;

  /* Starting point */
  xy = gsl_vector_alloc(2);
  gsl_vector_set(xy, 0, 5.0);
  gsl_vector_set(xy, 1, 7.0);

  /* Set initial step sizes to 1 */
  ss = gsl_vector_alloc(2);
  gsl_vector_set_all(ss, 1.0);

  /* Initialize method and iterate */
  minex_func.n = 2;
  minex_func.f = rosenbrock;
  minex_func.params = NULL;

  s = gsl_multimin_fminimizer_alloc(T, 2);
  gsl_multimin_fminimizer_set(s, &minex_func, xy, ss);

  do {
    iter++;
    status = gsl_multimin_fminimizer_iterate(s);

    if (status)
      break;

    size = gsl_multimin_fminimizer_size(s);
    status = gsl_multimin_test_size(size, 1e-3);

    if (status == GSL_SUCCESS) {
      printf ("converged to minimum at\n");
    }

    printf("iter = %3d,  x = %9.3e,  y = %9.3e,  f(x,y) = %6.3f\n",
            iter,
            gsl_vector_get(s->x, 0),
            gsl_vector_get(s->x, 1),
            s->fval);
  } while (status == GSL_CONTINUE && iter < 100);

  gsl_vector_free(xy);
  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free(s);

  return status;
}
