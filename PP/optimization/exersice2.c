#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multimin.h>

double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
int n = sizeof(t)/sizeof(t[0]);

// Function to fit
double fit_function(double A, double B, double T, double t) {
  return A*exp(-t/T)+B;
}

double summed_squares(const gsl_vector *v, void *params) {
  double A = gsl_vector_get(v, 0);
  double T = gsl_vector_get(v, 1);
  double B = gsl_vector_get(v, 2);
  double sum = 0;
  for (int i = 0; i < n; i++)
    sum += pow(fit_function(A,B,T,t[i]) - y[i], 2) / pow(e[i],2);
  return sum;
}

int main(void) {
  const gsl_multimin_fminimizer_type* T = gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer* s = NULL;
  gsl_vector* ss,* ATB;
  gsl_multimin_function minex_func;

  int iter = 0;
  int status;
  double size;

  /* Starting point */
  ATB = gsl_vector_alloc(3);
  gsl_vector_set(ATB, 0, 5.0);
  gsl_vector_set(ATB, 1, 7.0);
  gsl_vector_set(ATB, 2, 7.0);

  /* Set initial step sizes to 1 */
  ss = gsl_vector_alloc(3);
  gsl_vector_set_all(ss, 1.0);

  /* Initialize method and iterate */
  minex_func.n = 3;
  minex_func.f = summed_squares;
  minex_func.params = NULL;

  s = gsl_multimin_fminimizer_alloc(T, 3);
  gsl_multimin_fminimizer_set(s, &minex_func, ATB, ss);

  do {
    iter++;
    status = gsl_multimin_fminimizer_iterate(s);

    if (status)
      break;

    size = gsl_multimin_fminimizer_size(s);
    status = gsl_multimin_test_size(size, 1e-3);

    if (status == GSL_SUCCESS) {
      fprintf(stderr, "converged to minimum at\n");
      fprintf(stderr, "iter = %3d, %9.3e*exp(-t/%9.3e)+%9.3e,  residual error = %6.3f\n",
                      iter,
                      gsl_vector_get(s->x, 0),
                      gsl_vector_get(s->x, 1),
                      gsl_vector_get(s->x, 2),
                      s->fval);
      fprintf(stderr, "And so the lifetime is T = %g\n", gsl_vector_get(s->x, 1));
    }
  } while (status == GSL_CONTINUE && iter < 500);

  double A_fit = gsl_vector_get(s->x, 0),
         T_fit = gsl_vector_get(s->x, 1),
         B_fit = gsl_vector_get(s->x, 2);

  // Export the fitted model for the interval of the date
  printf("t f\n");
  double inc = (t[n-1]-t[0])/100;
  double extra = 2.0;
  for (double ti = t[0]-extra; ti < t[n-1]+extra; ti+=inc) {
    printf("%g %g\n", ti, fit_function(A_fit,B_fit,T_fit,ti));
  }

  gsl_vector_free(ATB);
  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free(s);
  fflush(stderr);

  return status;
}
