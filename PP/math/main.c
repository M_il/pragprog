#include <stdio.h>
#include <math.h>
#include <complex.h>

char* complex2str(double complex z) {
	char* buffer;
	asprintf(&buffer, "%g + %gi", creal(z), cimag(z));
	return buffer;
}

int main() {
	printf("Γ(5) = %g\n", tgamma(5));
	printf("J₁(0.5) = %g\n", j1(0.5));

	double complex z = csqrt(-2);
	printf("√(-2) = %s\n", complex2str(z));

	z = cpow(M_E, I);
	printf("e^i = %s\n", complex2str(z));

	z = cpow(M_E, I*M_PI);
	printf("e^(iπ) = %s\n", complex2str(z));

	z = cpow(I, M_E);
	printf("i^(e) = %s\n", complex2str(z));

	printf("\nNumber of significant digits:\n");

	float f 				= 0.1111111111111111111111111111f;
	double d 				= 0.1111111111111111111111111111;
	long double ld 	= 0.1111111111111111111111111111L;
	printf("float:\t\t%.25g\n", f);
	printf("double:\t\t%.25g\n", d);
	printf("long double:\t%.25Lg\n", ld);
}
