#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>

int u_ode(double x, const double u[], double dudx[], void *params) {
  dudx[0] = 2/sqrt(M_PI) * exp(-x*x);
  return GSL_SUCCESS;
}

double err_fun(double x) {
  gsl_odeiv2_system sys = {u_ode, NULL, 1, NULL};

  double acc=1e-6, eps=1e-6, hstart=copysign(0.1,x);
  gsl_odeiv2_driver* driver =
   gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rkf45,hstart,acc,eps);

  double x_start = 0; // Dynamic variables
  double u = 0;

  gsl_odeiv2_driver_apply(driver, &x_start, x, &u);
  gsl_odeiv2_driver_free(driver);

  return u;
}

int main(int argc, char const *argv[]) {
  assert(argc==4); // Must take 3 arguments as per 2nd exersice

  double a  = atof(argv[1]),
         b  = atof(argv[2]),
         dx = atof(argv[3]);

  for (double x = a; x <= b+dx; x += dx)
    printf("%.5e %.5e\n", x, err_fun(x));

  return 0;
}
