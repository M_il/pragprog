#import <stdio.h>
#import <math.h>

double cosine(double x);

int main(int argc, char const *argv[]) {
  printf("# x my_cos math_cos\n");
  for (double x = 0; x < 4*M_PI; x+=4*M_PI/100)
    printf("%2.4lf %1.4lf %1.4lf\n", x, cosine(x), cos(x));
  return 0;
}
