#include <stdio.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>

int cosine_ode(double x, const double y[], double dydx[], void *params) {
  dydx[0] = y[1];
  dydx[1] = - y[0];
  return GSL_SUCCESS;
}

// Solving y′′ =−y, y(0)=1, y′(0)=0.
double cosine(double x) {
  // Normalize input to 0 <= x < pi/2 and
  x = fmod(fabs(x),2*M_PI);         // x <- |x| mod 2π as cos is periodic
  int sign = 1;
  if (x > M_PI) {
    sign = -1;  // (cos(x) : x in [0,π]) = -(cos(x) : x in [π,2π])
    x-=M_PI;    // x <- x mod π
  }

  if (x > M_PI_2) {
    sign *= -1;         // (cos(x) : x in [0,π/2]) = - (cos(x) : x in [π,π/2]) i.e. mirrored
    x-=2*(x - M_PI_2);  // mirror x about π/2
  }

  gsl_odeiv2_system sys = {cosine_ode, NULL, 2, NULL};

  double acc=1e-6, eps=1e-6, hstart=1e-5;
  gsl_odeiv2_driver* driver =
    gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rkf45, hstart, acc, eps);

  double x_0 = 0.0;
  double y[] = {1.0,0.0};
  gsl_odeiv2_driver_apply(driver, &x_0, x, y);
  gsl_odeiv2_driver_free(driver);

  return sign*y[0];
}
