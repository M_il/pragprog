#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>

int y_ode(double x, const double y[], double dydx[], void *params) {
  dydx[0] = y[0] * (1 - y[0]);
  return GSL_SUCCESS;
}

int main(int argc, char const *argv[]) {
  // Solving y'(x) = y(x)*(1-y(x))
  // from x=0 to x=3 with the initial condition y(0)=0.5

  gsl_odeiv2_system sys = {y_ode, NULL, 1, NULL};

  gsl_odeiv2_driver* driver =
    gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk2, 1e-6, 1e-6, 0.0);

  const double x_start = 0.0, x_end = 3.0;
  const double step_size = 0.1;
  double x = x_start; // Dynamic variables
  double y = 0.5;

  for (double x_i = x_start; x_i <= x_end; x_i += step_size) {
    int status = gsl_odeiv2_driver_apply (driver, &x, x_i, &y);

    if (status != GSL_SUCCESS) {
        printf("error, return value=%d\n", status);
        break;
    }

    printf("%.5e %.5e\n", x, y);
  }

  return 0;
}
