#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_math.h>

/* Solving the system: u''(phi) + u'(phi) = 1 + epsilon*u^2(phi)
y0 = u
y1 = u'
then
y0' = y1
y1' = 1-y0+ε*y0*y0
*/
int u_ode(double x, const double y[], double dydt[], void *params) {
  double epsilon = *(double*)params;
  dydt[0] = y[1];
  dydt[1] = 1 - y[0] + epsilon * y[0]*y[0];
  return GSL_SUCCESS;
}

int main(int argc, char const *argv[]) {
  if (argc !=4) {
    fprintf(stderr, "Useage: ./orbit init_u init_u' epsilon\n");
    return EXIT_FAILURE;
  }
  double init_u = atof(argv[1]), init_up = atof(argv[2]);
  double epsilon = atof(argv[3]);

  gsl_odeiv2_system sys = {u_ode, NULL, 2, &epsilon};

  gsl_odeiv2_driver* driver =
    gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk2, 1e-6, 1e-6, 0.0);

  const int n_revs = 50;
  const double phi_start = 0.0, phi_end = n_revs * 2.0*M_PI;
  const double step_size = (phi_end - phi_start)/(n_revs * 100);
  double phi = phi_start; // Dynamic variables
  double y[2] = {init_u, init_up};

  for (double phi_i = phi_start; phi_i <= phi_end; phi_i += step_size) {
    //printf("phi=%g\n", phi_i);
    int status = gsl_odeiv2_driver_apply(driver, &phi, phi_i, y);
    //int status = gsl_odeiv2_driver_apply_fixed_step(driver, &phi, step_size, 1, y);

    if (status != GSL_SUCCESS) {
        printf("error, return value=%d\n", status);
        break;
    }

    // print (phi, u)
    printf("%.5e %.5e\n", phi, y[0]);
  }

  gsl_odeiv2_driver_free(driver);
  return EXIT_SUCCESS;
}
