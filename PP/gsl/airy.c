#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_sf_airy.h>

int main(int argc, char** argv) {
  double x_min = atof(argv[1]);
  double x_max = atof(argv[2]);

  for (double x = x_min; x < x_max; x+=0.1) {
    double Ai_x = gsl_sf_airy_Ai(x, GSL_PREC_DOUBLE);
    double Bi_x = gsl_sf_airy_Bi(x, GSL_PREC_DOUBLE);
    printf("%g\t%g\t%g\n", x, Ai_x, Bi_x);
  }
}
