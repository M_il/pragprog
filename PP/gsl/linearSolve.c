#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>

/* We must solve the following set of linear equations Ax = b as
[	6.13	-2.90	5.86	]	[x0]		[6.23]
[	8.08	-6.31	-3.89	]	[x1]	=	[5.37]
[	-4.36	1.00	0.19	]	[x2]		[2.29]
*/
double matrixData[] = {6.13, -2.90, 5.86,
                      8.08, -6.31, -3.89,
                      -4.36, 1.00, 0.19};

double resultData[] = {6.23, 5.37, 2.29};

int main() {
  // Create matrix
  gsl_matrix_view A_view = gsl_matrix_view_array(matrixData, 3, 3);
  gsl_matrix* A = &A_view.matrix;

  // Create result-vector
  gsl_vector_view b_view = gsl_vector_view_array(resultData, 3);
  gsl_vector* b = &b_view.vector;

  // Create solution vector
  gsl_vector* x = gsl_vector_alloc(3);

  // Solve using a temporary matrix not to destroy A
  gsl_matrix* T = gsl_matrix_alloc(3, 3);
  gsl_matrix_memcpy(T, A);
  gsl_linalg_HH_solve(T,b,x);
  gsl_matrix_free(T);

  printf("The solution is x =\n");
  gsl_vector_fprintf(stdout, x, "%g");
  printf("\n");

  // Test if it worked
  gsl_vector* Ax = gsl_vector_alloc(3);
  gsl_blas_dgemv(CblasNoTrans, 1, A, x, 0, Ax);

  printf("Ax =\n");
  gsl_vector_fprintf(stdout, Ax, "%g");
  printf("\n");

  printf("Shound be equal to b =\n");
  gsl_vector_fprintf(stdout, b, "%g");
  printf("\n");
  
  gsl_vector_free(x);

  return 0;
}
