#include "newton.h"
#include <qr.h>
#include <type_print.h>

// f(x,y)=(1-x)^2+100(y-x^2)^2
double f_rosenbrock(gsl_vector* vx, gsl_vector* df, gsl_matrix* H) {
  double y = gsl_vector_get(vx,1),
         x = gsl_vector_get(vx,0);

  // According to WolframAlpha
  if (df != NULL) {
    double
    dfdx   = 2*(200*x*x*x - 200*x*y + x - 1),
    dfdy   = 200*(y - x*x);
    gsl_vector_set(df,0,dfdx);gsl_vector_set(df,1,dfdy);
  }
  if (H != NULL) {
    double
    dfdxdx = 1200*x*x - 400*y + 2,
    dfdxdy = -400*x,
    dfdydx = dfdxdy,
    dfdydy = 200;
    gsl_matrix_set(H,0,0,dfdxdx);gsl_matrix_set(H,0,1,dfdxdy);
    gsl_matrix_set(H,1,0,dfdydx);gsl_matrix_set(H,1,1,dfdydy);
  }
  return (1+x)*(1+x) + 100*(y-x*x)*(y-x*x);
}

// f(x,y)=(x^2+y-11)^2+(x+y^2-7)^2
double f_himmelblau(gsl_vector* vx, gsl_vector* df, gsl_matrix* H) {
  double y = gsl_vector_get(vx,1),
         x = gsl_vector_get(vx,0);

  // According to WolframAlpha
  if (df != NULL) {
    double
    dfdx   = 2*(2*x*(x*x+y-11) + x + y*y - 7),
    dfdy   = 2*(x*x + 2*y*(x+y*y-7) + y - 11);
    gsl_vector_set(df,0,dfdx);gsl_vector_set(df,1,dfdy);
  }
  if (H != NULL) {
    double
    dfdxdx = 12*x*x + 4*y - 42,
    dfdxdy = 4*(x+y),
    dfdydx = dfdxdy,
    dfdydy = 4*x + 12*y*y - 26;
    gsl_matrix_set(H,0,0,dfdxdx);gsl_matrix_set(H,0,1,dfdxdy);
    gsl_matrix_set(H,1,0,dfdydx);gsl_matrix_set(H,1,1,dfdydy);
  }
  return (x*x+y-11)*(x*x+y-11) + (x+y*y-7)*(x+y*y-7);
}

int main(int argc, char const *argv[]) {
  int steps;

  printf("Newton's minimization method with back-tracking line-search\n\n");
  gsl_vector* x = gsl_vector_alloc(2);
  double eps = 1e-3;

  // ---- Rosenbrock ----
  printf("Rosenbrock function (has only one minimum):\n");
  gsl_vector_set(x,0,0.5);gsl_vector_set(x,1,0.5);
  steps = newton(f_rosenbrock, x, eps);
  printf("Minimum at: [x,y] = "); printv(x);
  printf("Steps to converge: %i\n", steps);
  printf("\n");

  // ---- Himmelblau ----
  printf("Himmelblau function (has 4 minima):\n");
  gsl_vector_set(x,0,3.5);gsl_vector_set(x,1,2.5);
  steps = newton(f_himmelblau, x, eps);
  printf("Minimum 1 at: [x,y] = "); printv(x);
  printf("Steps to converge: %i\n", steps);

  gsl_vector_set(x,0,-3);gsl_vector_set(x,1,-3);
  steps = newton(f_himmelblau, x, eps);
  printf("Minimum 2 at: [x,y] = "); printv(x);
  printf("Steps to converge: %i\n", steps);

  gsl_vector_set(x,0,-2);gsl_vector_set(x,1,3);
  steps = newton(f_himmelblau, x, eps);
  printf("Minimum 3 at: [x,y] = "); printv(x);
  printf("Steps to converge: %i\n", steps);

  gsl_vector_set(x,0,3);gsl_vector_set(x,1,-2);
  steps = newton(f_himmelblau, x, eps);
  printf("Minimum 4 at: [x,y] = "); printv(x);
  printf("Steps to converge: %i\n", steps);

  printf("This method performs the worst.\n");

  return 0;
}
