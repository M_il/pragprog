#import "newton.h"

#define ALPHA 1e-4
#define MIN_LAMB 1.0/64

int newton(
  /* f: objective function, df: gradient, H: Hessian matrix H*/
  double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H),
  /* starting point, becomes the latest approximation to the root on exit */
  gsl_vector* xstart,
  /* accuracy goal, on exit |gradient|<eps */
  double eps
) {
  int steps = 0;
  int dim = xstart->size;

  gsl_vector* x = xstart;
  gsl_vector* x_plus_s = gsl_vector_alloc(dim);
  gsl_vector* df = gsl_vector_alloc(dim);
  gsl_matrix* H = gsl_matrix_alloc(dim,dim);
  gsl_matrix* R = gsl_matrix_alloc(dim,dim);
  gsl_matrix* H_inv = gsl_matrix_alloc(dim,dim);
  gsl_vector* delta_x = gsl_vector_alloc(dim);

  do {
    f(x,df,H);
    // Inverte matrix H
    qr_gs_decomp(H,R);
    gsl_matrix* Q = H;
    qr_gs_inverse(Q,R,H_inv);

    // delta_x = 1 * H_inv * df + 0*delta_x
    gsl_blas_dgemv(CblasNoTrans, 1, H_inv, df, 0, delta_x);

    // Find the propper s (lamb)
    double lamb = 1;
    gsl_vector* s = delta_x;  // Use this memory for s
    double s_dot_df;
    while (true) {
      gsl_blas_dcopy(s,x_plus_s);     // x_plus_s <- x+s
      gsl_blas_daxpy(1,x,x_plus_s);   //
      gsl_blas_ddot(s,df,&s_dot_df);  // s_dot_df <- s*df

      if (f(x_plus_s,NULL,NULL) < f(x,NULL,NULL) + ALPHA*s_dot_df || lamb < MIN_LAMB) {
        break;
      }
      lamb *= 0.5;
      gsl_blas_dscal(0.5,s);          // s <- lamb*s
    }

    gsl_blas_daxpy(-1,s,x); // x <- x - s
    steps++;
  } while (gsl_blas_dnrm2(df) > eps);

  gsl_vector_free(x_plus_s);gsl_vector_free(df);gsl_vector_free(delta_x);
  gsl_matrix_free(H);gsl_matrix_free(R);gsl_matrix_free(H_inv);

  return steps;
}
