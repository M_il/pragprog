#include "qnewton.h"

double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
int N = sizeof(t)/sizeof(t[0]);

double f(double t, double A, double B, double T) {
  return A*exp(-t/T)+B;
}

// Weighed squared defference
double master(gsl_vector *params) {
  double A = gsl_vector_get(params,0),
         B = gsl_vector_get(params,1),
         T = gsl_vector_get(params,2);
  double s = 0;
  for (int i = 0; i < N;i++) {
    double diff = (f(t[i],A,B,T)-y[i]);
    s += (diff*diff)/(e[i]*e[i]);
  }
  return s;
}

int main(int argc, char const *argv[]) {

  gsl_vector *x = gsl_vector_alloc(3);
  gsl_vector_set(x,0,4);
  gsl_vector_set(x,1,0);
  gsl_vector_set(x,2,6);
  double acc = 1e-3;
  qnewton(master, x, acc);

  printf("# Data: t y err\n");
  for (int i = 0; i < N; i++) {
    printf("%g %g %g\n", t[i], y[i], e[i]);
  }

  printf("\n\n");

  printf("# Fit: t f(t)\n");
  for (double ti = t[0]-1; ti < t[N-1]+1; ti+=(t[N]-t[0])/100) {
    double A = gsl_vector_get(x,0),
           B = gsl_vector_get(x,1),
           T = gsl_vector_get(x,2);
    printf("%g %g\n", ti, f(ti,A,B,T));
  }

  gsl_vector_free(x);

  return 0;
}
