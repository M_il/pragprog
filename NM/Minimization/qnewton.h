#ifndef QNEWTON_H
#define QNEWTON_H

#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<stdbool.h>

int qnewton(double f(gsl_vector*), gsl_vector*x, double acc);
#endif
