#include "qnewton.h"
#include <type_print.h>

double f_rosenbrock(gsl_vector* vx) {
  double y = gsl_vector_get(vx,1),
         x = gsl_vector_get(vx,0);
  return (1-x)*(1-x) + 100*(y-x*x)*(y-x*x);
}

double f_himmelblau(gsl_vector* vx) {
  double y = gsl_vector_get(vx,1),
         x = gsl_vector_get(vx,0);
  return (x*x+y-11)*(x*x+y-11) + (x+y*y-7)*(x+y*y-7);
}

int main(int argc, char const *argv[]) {
  int steps;

  printf("Quasi-Newton method with Broyden's update\n\n");
  gsl_vector* x = gsl_vector_alloc(2);
  double acc = 1e-3;

  // ---- Rosenbrock ----
  printf("Rosenbrock function (has only one minimum):\n");
  gsl_vector_set(x,0,0.5);gsl_vector_set(x,1,0.5);
  steps = qnewton(f_rosenbrock, x, acc);
  printf("Minimum at: [x,y] = "); printv(x);
  printf("Steps to converge: %i\n", steps);
  printf("\n");

  // ---- Himmelblau ----
  printf("Himmelblau function (has 4 minima):\n");
  gsl_vector_set(x,0,3.5);gsl_vector_set(x,1,2.5);
  steps = qnewton(f_himmelblau, x, acc);
  printf("Minimum 1 at: [x,y] = "); printv(x);
  printf("Steps to converge: %i\n", steps);

  gsl_vector_set(x,0,-3);gsl_vector_set(x,1,-3);
  steps = qnewton(f_himmelblau, x, acc);
  printf("Minimum 2 at: [x,y] = "); printv(x);
  printf("Steps to converge: %i\n", steps);

  gsl_vector_set(x,0,-2);gsl_vector_set(x,1,3);
  steps = qnewton(f_himmelblau, x, acc);
  printf("Minimum 3 at: [x,y] = "); printv(x);
  printf("Steps to converge: %i\n", steps);

  gsl_vector_set(x,0,3);gsl_vector_set(x,1,-2);
  steps = qnewton(f_himmelblau, x, acc);
  printf("Minimum 4 at: [x,y] = "); printv(x);
  printf("Steps to converge: %i\n", steps);

  printf("This performs the best in the mean.\n");

  return 0;
}
