#include <type_print.h>

// Newton-method from root-finding exersice
int newton(int f(const gsl_vector* x, gsl_vector* fx), gsl_vector* x, double dx, double eps);

int f_rosenbrock(const gsl_vector* vx, gsl_vector* fx) {
  double y = gsl_vector_get(vx,1),
         x = gsl_vector_get(vx,0);
  double
  dfdx   = 2*(200*x*x*x - 200*x*y + x - 1),
  dfdy   = 200*(y - x*x);
  gsl_vector_set(fx,0,dfdx);gsl_vector_set(fx,1,dfdy);
  return 0;
}

int f_himmelblau(const gsl_vector* vx, gsl_vector* fx) {
  double y = gsl_vector_get(vx,1),
         x = gsl_vector_get(vx,0);
  double
  dfdx   = 2*(2*x*(x*x+y-11) + x + y*y - 7),
  dfdy   = 2*(x*x + 2*y*(x+y*y-7) + y - 11);
  gsl_vector_set(fx,0,dfdx);gsl_vector_set(fx,1,dfdy);
  return 0;
}

int main(int argc, char const *argv[]) {
  int steps;

  printf("Newton method as of root-finding exercise (numeric Jacobian)\n\n");
  gsl_vector* x = gsl_vector_alloc(2);
  double dx = 1e-6, eps = 1e-3;

  // ---- Rosenbrock ----
  printf("Rosenbrock function (has only one minimum):\n");
  gsl_vector_set(x,0,0.5);gsl_vector_set(x,1,0.5);
  steps = newton(f_rosenbrock, x, dx, eps);
  printf("Minimum at: [x,y] = "); printv(x);
  printf("Steps to converge: %i\n", steps);
  printf("\n");

  // ---- Himmelblau ----
  printf("Himmelblau function (has 4 minima):\n");
  gsl_vector_set(x,0,3.5);gsl_vector_set(x,1,2.5);
  steps = newton(f_himmelblau, x, dx, eps);
  printf("Minimum 1 at: [x,y] = "); printv(x);
  printf("Steps to converge: %i\n", steps);

  gsl_vector_set(x,0,-3);gsl_vector_set(x,1,-3);
  steps = newton(f_himmelblau, x, dx, eps);
  printf("Minimum 2 at: [x,y] = "); printv(x);
  printf("Steps to converge: %i\n", steps);

  gsl_vector_set(x,0,-2);gsl_vector_set(x,1,3);
  steps = newton(f_himmelblau, x, dx, eps);
  printf("Minimum 3 at: [x,y] = "); printv(x);
  printf("Steps to converge: %i\n", steps);

  gsl_vector_set(x,0,3);gsl_vector_set(x,1,-2);
  steps = newton(f_himmelblau, x, dx, eps);
  printf("Minimum 4 at: [x,y] = "); printv(x);
  printf("Steps to converge: %i\n", steps);

  printf("This performs the next best.\n");

  return 0;
}
