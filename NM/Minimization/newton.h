#ifndef NEWTON_H
#define NEWTON_H

#import <gsl/gsl_matrix.h>
#import <gsl/gsl_vector.h>
#import <gsl/gsl_blas.h>
#import <qr.h>
#import <stdbool.h>

void newton_reset_steps();
int get_newton_steps();
int newton(
  /* f: objective function, df: gradient, H: Hessian matrix H*/
  double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H),
  /* starting point, becomes the latest approximation to the root on exit */
  gsl_vector* xstart,
  /* accuracy goal, on exit |gradient|<eps */
  double eps
);
#endif
