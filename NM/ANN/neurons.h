#ifndef NEURONS_H
#define NEURONS_H
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
#include<stdio.h>
#include<math.h>
typedef struct {
  int n;              // Number of neurons
  double(*f)(double); // Activation function
  gsl_vector* data;   // a,b,c vectors
} neurons;
neurons* neurons_alloc(int n,double(*f)(double));
void neurons_free(neurons* nw);
double neurons_feed_forward(neurons* nw,double x);
void neurons_train(neurons* nw,gsl_vector* xlist,gsl_vector* ylist);
#endif
