#include "ann.h"
#define RND (double)rand()/RAND_MAX

ann* ann_alloc(int n_input, int n_hidden_neurons, int n_output, double(*activation_function)(double)) {
  ann* nw = malloc(sizeof(ann));
	nw->f=activation_function;
	nw->params=gsl_vector_alloc(
    n_input*n_hidden_neurons +  // A
    n_hidden_neurons +          // b
    n_hidden_neurons*n_output   // W
  );
  // Initial values of network params
  for (size_t i = 0; i < nw->params->size; i++) {
    gsl_vector_set(nw->params, i, RND);
  }
  double *data_ptr = nw->params->data;
  nw->Av = gsl_matrix_view_array(data_ptr, n_hidden_neurons, n_input);  // In Ax
  data_ptr += n_hidden_neurons*n_input;
  nw->bv = gsl_vector_view_array(data_ptr, n_hidden_neurons);           // In Ax + b
  data_ptr += n_hidden_neurons;
  nw->Wv = gsl_matrix_view_array(data_ptr, n_output, n_hidden_neurons); // In W f(Ax + b)
	return nw;
}

void ann_free(ann* nw) {
  gsl_vector_free(nw->params);
}

void ann_feed_forward(ann* nw, gsl_vector *x, gsl_vector *y) {
  int n_hidden_neurons = nw->Av.matrix.size1;

  gsl_vector *t = gsl_vector_alloc(n_hidden_neurons);
  gsl_vector_memcpy(t,&(nw->bv.vector));
  gsl_blas_dgemv(CblasNoTrans, 1, &(nw->Av.matrix), x, 0, t); // t <- Ax + b
  for (size_t i = 0; i < n_hidden_neurons; i++) {             // t <- f(Ax + b)
    gsl_vector_set(t, i, nw->f(gsl_vector_get(t,i)));
  }
  gsl_blas_dgemv(CblasNoTrans, 1, &(nw->Wv.matrix), t, 0, y); // y <- W f(Ax + b)
  gsl_vector_free(t);
}

void _optimize_network(gsl_vector* p,
                       double (*delta)(const gsl_vector* p, void* params)) {
  int problem_dim = p->size;
  gsl_vector* step_size = gsl_vector_alloc(problem_dim);
  gsl_vector_set_all(step_size, 1.0);
  gsl_multimin_function func;
  func.n = problem_dim;
  func.f = delta;
  func.params = NULL;

  double del = delta(p,NULL);
  fprintf(stderr, "p[0] = %g, dell = %g\n", gsl_vector_get(p,0), del);
  gsl_blas_dscal(2,p);
  del = delta(p,NULL);
  fprintf(stderr, "p[0] = %g, dell = %g\n", gsl_vector_get(p,0), del);

  const gsl_multimin_fminimizer_type *type = gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer *s = gsl_multimin_fminimizer_alloc(type, problem_dim);
  gsl_multimin_fminimizer_set(s, &func, p, step_size);

  size_t iter = 0;
  int status;
  do {
    iter++;
    status = gsl_multimin_fminimizer_iterate(s);

    if (status)
      break;

    double size = gsl_multimin_fminimizer_size(s);
    status = gsl_multimin_test_size(size, 1e-5);

    if (iter % 500 == 0)
      fprintf(stderr, "step = %li, loss = %g, p = {%g %g %g ...}\n", iter, s->fval, gsl_vector_get(s->x,0), gsl_vector_get(s->x,1), gsl_vector_get(s->x,2));
    if (status == GSL_SUCCESS) {
      fprintf(stderr, "Converged!\n");
    }
  } while (status == GSL_CONTINUE && iter < 5e4);

  // Copy the result into p
  gsl_vector_memcpy(p,s->x);

  gsl_vector_free(step_size);
  gsl_multimin_fminimizer_free(s);
}

void ann_train(ann* nw, int n_samples, gsl_vector* x[], gsl_vector* y[], double (*loss)(gsl_vector *y, gsl_vector *y_nn)) {
  // Loos function: sum of squared errors
  int n_output = y[0]->size;
  gsl_vector *xi, *yi;
  gsl_vector *y_nn = gsl_vector_alloc(n_output);
  double delta(const gsl_vector* p, void* params){
    gsl_vector_memcpy(nw->params,p);
    // fprintf(stderr, "p[0] = %g\n", gsl_vector_get(nw->params,0));
    double s=0;
    for(int i=0;i<n_samples;i++){ // Use all training samples
      xi = x[i];
      yi = y[i];
      ann_feed_forward(nw,xi,y_nn);
      // fprintf(stderr, "xi = %g, yi = %g\n", gsl_vector_get(xi,0), gsl_vector_get(yi,0));
      // fprintf(stderr, "y_nn = %g\n", gsl_vector_get(y_nn,0));
      s+=loss(yi,y_nn);
    }
    // fprintf(stderr, "s/n = %g\n", s/n_samples);
    return s/n_samples;
  }

  // Minimize the error using GSL minimization routine
  _optimize_network(nw->params, delta);
  gsl_vector_free(y_nn);
}
