#include "ann.h"

double activation_function(double x){
  return log(1+exp(x)); // Softplus (smooth ReLU)
}

double function_to_fit(double x1, double x2){
  return x1*x2;
}

// Loss function (trying to speed things up)
double loss_function(gsl_vector *y, gsl_vector *y_nn) {
  double result = 0;
  double* y_v = gsl_vector_ptr(y, y->size-1);
  double* y_nn_v = gsl_vector_ptr(y_nn, y_nn->size-1);
  for (int i = 0;i < y->size; i++) {
    result += pow(y_v[i] - y_nn_v[i],2);
  }
  return sqrt(result);
}

int main(int argc, char const *argv[]) {
  int n_input = 2, n_hidden_neurons = 10, n_output = 1;
  ann *nw = ann_alloc(n_input, n_hidden_neurons, n_output, activation_function);

  // Cleate the data
  int d_samples = 30;
  int n_samples = d_samples*d_samples;
  gsl_vector *x[n_samples];
  gsl_vector *y[n_samples];
  for (int i = 0; i < n_samples; i++) {
    x[i] = gsl_vector_alloc(n_input);
    y[i] = gsl_vector_alloc(n_output);
  }
  int x_index = 0;
  for (double x1 = -3; x1 < 3; x1 += 6.0/d_samples) {
    for (double x2 = -3; x2 < 3; x2 += 6.0/d_samples) {
      gsl_vector_set(x[x_index],0,x1);
      gsl_vector_set(x[x_index],1,x2);
      double yi = function_to_fit(x1,x2);
      gsl_vector_set(y[x_index],0,yi);
      printf("%g %g %g\n", x1,x2,yi);
      x_index++;
    }
  }
  // Train the network
  ann_train(nw, n_samples, x, y, loss_function);

  printf("\n\n");

  // Print the ann interplation
  gsl_vector *y_nn = gsl_vector_alloc(1);
  for (int i = 0; i < n_samples; i++) {
    ann_feed_forward(nw, x[i], y_nn);
    printf("%g %g %g\n", gsl_vector_get(x[i],0), gsl_vector_get(x[i],1), gsl_vector_get(y_nn,0));
  }

  return 0;
}
