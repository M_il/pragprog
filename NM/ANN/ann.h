#ifndef ANN_H
#define ANN_H
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<math.h>
typedef struct { // ANN of the form W*f(A*x + b) (* is matrix multiply)
  gsl_matrix_view Av;      // data will hold the data of A,b and W
  gsl_vector_view bv;
  gsl_matrix_view Wv;
  gsl_vector *params;
  double(*f)(double);
} ann;
ann* ann_alloc(int n_input, int n_hidden_neurons, int n_output, double(*activation_function)(double));
void ann_free(ann* network);
void ann_feed_forward(ann* nw, gsl_vector *x, gsl_vector *y);
void ann_train(ann* nw, int n_samples, gsl_vector* x[], gsl_vector* y[], double (*loss)(gsl_vector *y, gsl_vector *y_nn));
#endif
