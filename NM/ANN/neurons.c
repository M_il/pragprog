#include"neurons.h"

// Allocate a number of neurons (one layer)
// To store a,b,w in
//   y = sum_i wi f((x + ai)/bi)
neurons* neurons_alloc(int n,double(*f)(double)){
	neurons* nw = malloc(sizeof(neurons));
	nw->n=n;
	nw->f=f;
	nw->data=gsl_vector_alloc(3*n);
	return nw;
}

// Free the network
void neurons_free(neurons* nw){
	gsl_vector_free(nw->data);
	free(nw);
}

// Calculate the output
double neurons_feed_forward(neurons* nw,double x){
	double s=0;
	for(int i=0;i<nw->n;i++){
		double a=gsl_vector_get(nw->data,0*nw->n+i);
		double b=gsl_vector_get(nw->data,1*nw->n+i);
		double w=gsl_vector_get(nw->data,2*nw->n+i);
		s+=nw->f((x+a)/b)*w;
	}
	return s;
}

void _optimize_network(gsl_vector* p,
                       double (*delta)(const gsl_vector* p, void* params))
{
  int problem_dim = p->size;
  gsl_vector* step_size = gsl_vector_alloc(problem_dim);
  gsl_vector_set_all(step_size, 1.0);
  gsl_multimin_function func;
  func.n = problem_dim;
  func.f = delta;
  func.params = NULL;

  const gsl_multimin_fminimizer_type *type = gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer *s = gsl_multimin_fminimizer_alloc(type, problem_dim);
  gsl_multimin_fminimizer_set(s, &func, p, step_size);

  size_t iter = 0;
  int status;
  do {
    iter++;
    status = gsl_multimin_fminimizer_iterate(s);

    if (status)
      break;

    double size = gsl_multimin_fminimizer_size(s);
    status = gsl_multimin_test_size(size, 1e-5);

    // fprintf(stderr, "loss = %g, p[0] = %g\n", s->fval, gsl_vector_get(s->x,1));
    if (status == GSL_SUCCESS) {
      fprintf(stderr, "Converged!\n");
    }
  } while (status == GSL_CONTINUE && iter < 1e10);

  // Copy the result into p
  gsl_vector_memcpy(p,s->x);

  gsl_vector_free(step_size);
  gsl_multimin_fminimizer_free(s);
}

// 'Train' the network by minimization of a loss function delta
void neurons_train(neurons* nw,gsl_vector* vx,gsl_vector* vf){
  // Loos function: sum of squared errors
  double delta(const gsl_vector* p, void* params){
    gsl_vector_memcpy(nw->data,p);
    double s=0;
    for(int i=0;i<vx->size;i++){
      double x=gsl_vector_get(vx,i);
      double f=gsl_vector_get(vf,i);
      double y=neurons_feed_forward(nw,x);
      s+=fabs(y-f);
    }
    return s/vx->size;
  }

  // Minimize the error using GSL minimization routine
  _optimize_network(nw->data, delta);
}
