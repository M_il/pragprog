#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include"neurons.h"

double activation_function(double x){
  return x*exp(-x*x);
}
double function_to_fit(double x){
  return cos(5*x-1)*exp(-x*x);
}

int main(int argc, char const *argv[]) {
	int n = (argc == 2) ? atof(argv[1]) : 5;
	neurons* nw=neurons_alloc(n,activation_function);

	// Generate some data to fit to
	double a=-1,b=1;
	int nx=20;
	gsl_vector* vx=gsl_vector_alloc(nx);
	gsl_vector* vf=gsl_vector_alloc(nx);
	for(int i=0;i<nx;i++){
		double x=a+(b-a)*i/(nx-1);
		double f=function_to_fit(x);
		gsl_vector_set(vx,i,x);
		gsl_vector_set(vf,i,f);
	}
	for(int i=0;i<nw->n;i++){
		gsl_vector_set(nw->data,0*nw->n+i,a+(b-a)*i/(nw->n-1));
		gsl_vector_set(nw->data,1*nw->n+i,1);
		gsl_vector_set(nw->data,2*nw->n+i,1);
	}

	neurons_train(nw,vx,vf);

    // Print the data and the fitted curve
	for(int i=0;i<vx->size;i++){
		double x=gsl_vector_get(vx,i);
		double f=gsl_vector_get(vf,i);
		printf("%g %g\n",x,f);
	}
	printf("\n\n");

	double dz=1.0/64;
	for(double z=a;z<=b;z+=dz){
		double y=neurons_feed_forward(nw,z);
		printf("%g %g\n",z,y);
	}

  neurons_free(nw);
  return 0;
}
