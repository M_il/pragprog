#include <math.h>
#include <stdio.h>
#include "plainmc.h"

double f_test_last(double *x) {
  return 1/(
            (1-cos(x[0])*cos(x[1])*cos(x[2])) * pow(M_PI,3)
           );
}

double f_test1(double *x) {
  return log(1 + x[0])/x[0];
}

int main(int argc, char const *argv[]) {
  // double a_last[] = {0,0,0};
  // double b_last[] = {M_PI,M_PI,M_PI};
  // double result, error;
  // double exact = pow(tgamma(1.0/4),4)/(4*pow(M_PI,3));
  // // Using the function from problem A
  // for (int N = 1; N < 100; N+=1) {
  //   plainmc(3, a_last, b_last, &f_test_last, N, &result, &error);
  //   printf("%i %g\n", N, fabs(result-exact));
  // }

  double a[] = {0}, b[] = {1};
  double result, error;
  double exact = M_PI*M_PI / 12; // Shaums 18.93
  for (int N = 5; N < 2e4; N++) {
    plainmc(1, a, b, &f_test1, N, &result, &error);
    printf("%i %g\n", N, fabs(result-exact));
  }

  return 0;
}
