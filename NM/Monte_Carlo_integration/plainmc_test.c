#include <math.h>
#include <stdio.h>
#include "plainmc.h"

double f_test1(double *x) {
  return log(1 + x[0])/x[0];
}

double f_sphere(double *x) {
  if (x[0]*x[0] + x[1]*x[1] + x[2]*x[2] <= 1)
    return 1;
  else
    return 0;
}

double f_test_last(double *x) {
  return 1/(
            (1-cos(x[0])*cos(x[1])*cos(x[2])) * pow(M_PI,3)
           );
}

int main(int argc, char const *argv[]) {
  int N = 1000;
  double a[] = {0}, b[] = {1};
  double result, error;
  double exact = M_PI*M_PI / 12; // Shaums 18.93
  printf("Integrating ln(1+x)/x from 0 to 1\n");
  plainmc(1, a, b, &f_test1, N, &result, &error);
  printf("N               = %i\n", N);
  printf("result          = %.6f\n", result);
  printf("exact = π^2/12  = %.6f\n", exact);
  printf("estimated error = %e\n", error);
  printf("actual error    = %e\n", fabs(result-exact));

  N = 1e7;
  double a_sphere[] = {-1,-1,-1};
  double b_sphere[] = {1,1,1};
  exact = 4*M_PI/3;
  printf("\n");
  printf("Integrating to calculate volume of sphere:\n"
         "1 if x^2+y^2+z^2 <= 1, otherwise 0\n"
         "from -1 to 1 in all x,y and z\n");
  plainmc(3, a_sphere, b_sphere, &f_sphere, N, &result, &error);
  printf("N               = %i\n", N);
  printf("result          = %.6f\n", result);
  printf("exact = 4/3 π   = %.6f\n", exact);
  printf("estimated error = %e\n", error);
  printf("actual error    = %e\n", fabs(result-exact));

  N = 5e7;
  double a_last[] = {0,0,0};
  double b_last[] = {M_PI,M_PI,M_PI};
  exact = pow(tgamma(1.0/4),4)/(4*pow(M_PI,3));
  printf("\n");
  printf("Integrating:\n"
         "∫_0^π dx/π ∫_0^π dy/π ∫_0^π dz/π [1-cos(x)cos(y)cos(z)]^(-1)\n");
  plainmc(3, a_last, b_last, &f_test_last, N, &result, &error);
  printf("N                       = %i\n", N);
  printf("result                  = %.6f\n", result);
  printf("exact = Γ(1/4)^4/(4π^3) = %.6f\n", exact);
  printf("estimated error         = %e\n", error);
  printf("actual error            = %e\n", fabs(result-exact));

  return 0;
}
