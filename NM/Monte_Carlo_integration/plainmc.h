#ifndef PLAINMC_H
#define PLAINMC_H

void plainmc(int dim,               // Dimentionality of problem
             double *a,             // Lower limits of integration
             double *b,             // Upper limits of integration
             double f(double *x),   // Function to integrate
             int N,                 // Number of evaluations to make
             double* result,        // Pointer to hold the result
             double* error);        // Pointer to hold the error

void plainmc2D(double a,                    // Lower limit (x-direction)
               double b,                    // Upper limit (x-direction)
               double c(double),            // Lower limit (y-direction)
               double d(double),            // Upper limit (y-direction)
               double f(double x,double y), // Function to integrate
               int N,                       // Number of evaluations to make
               double* result,              // Pointer to hold the result
               double* error);              // Pointer to hold the error

#endif
