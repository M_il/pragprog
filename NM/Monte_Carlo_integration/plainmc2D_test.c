#include "plainmc.h"
#include <math.h>
#include <stdio.h>

// c and d makes out two half circles, and f the dome hight in z direction
double c_dome(double x) {
  return -sqrt(1-x*x);
}
double d_dome(double x) {
  return -c_dome(x);
}
double f_dome(double x, double y) {
  return sqrt(1 - x*x - y*y);
}

/*
  x
  |   ** d = 2x
  |  * *
  | *  *
  |*   *
  a----b--------->
  |*   *
  |  * *
  |    * c = -x
  |
*/
double c_prism(double x) {
  return -x;
}
double d_prism(double x) {
  return 2*x;
}
double f_prism(double x, double y) {
  return 1;
}

int main(int argc, char const *argv[]) {
  double result, error;
  double exact;
  double a,b;
  int N;

  printf("Itegration by recursively caling plainmc.\n\n");
  N = 1e8;
  a = -1; b = 1;
  exact = 4*M_PI/6;
  printf("Integrating to calculate volume of dome (half sphere):\n"
         "The usual x^2 + y^2 + z^2 = 1 has been rewritten to:\n"
         "sqrt(1 - x^2 - y^2) integrated from -1 to 1 in x and\n"
         "from -sqrt(1-x^2) to sqrt(1-x^2) in y.\n");
  plainmc2D(a,b,c_dome,d_dome,f_dome,N,&result,&error);
  printf("N               = %i\n", N);
  printf("result          = %.6f\n", result);
  printf("exact = 4/6 π   = %.6f\n", exact);
  printf("estimated error = %e\n", error);
  printf("actual error    = %e\n", fabs(result-exact));

  N = 1e8;
  a = 0; b = 1;
  exact = 0.5*(b-a)*(d_prism(b)-c_prism(b));
  printf("\n");
  printf("Integrating to calculate volume of triangular prism\n"
         "bounded by -x and 2x in y direction, and 0 and 1 in x direction.\n"
         "Its height is 1.\n");
  plainmc2D(a,b,c_prism,d_prism,f_prism,N,&result,&error);
  printf("N               = %i\n", N);
  printf("result          = %.6f\n", result);
  printf("exact           = %.6f\n", exact);
  printf("estimated error = %e\n", error);
  printf("actual error    = %e\n", fabs(result-exact));

  return 0;
}
