#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#define RND ((double)rand()/RAND_MAX)

// Choses a random x vector within the aux. rectangular volume, V,
// enclosing the integration region befined by the limits of the integration.
void random_x(int dim, double *a, double *b, double *x) {
  for (int i=0;i<dim; i++)
    x[i] = a[i] + RND*(b[i]-a[i]);
}

void plainmc(int dim,               // Dimentionality of problem
             double *a,             // Lower limits of integration
             double *b,             // Upper limits of integration
             double f(double *x),   // Function to integrate
             int N,                 // Number of evaluations to make
             double* result,        // Pointer to hold the result
             double* error) {       // Pointer to hold the error

  // Volume of the aux. integration space
  double V = 1;
  for (int i=0;i<dim;i++)
    V*=b[i]-a[i];

  double  sum=0,
          sum2=0,
          fx,
          x[dim];

  // For all number of eval points N, choose a random x in V
  //  calculate the sum of f(x) in eq. 2
  //  calculate the sum (sum2) of (f(x))^2 to be usid for sigma error estimate
  for (int i=0;i<N; i++) {
    random_x(dim,a,b,x);
    fx=f(x);
    sum+=fx;
    sum2+=fx*fx;
  }

  // Calculate the results according to eq. 2,
  // the error according to eq. 3 and 4
  double  avr = sum/N,
          var = sum2/N-avr*avr;
          *result = avr*V;
          *error = sqrt(var/N)*V;
}

void plainmc2D(double a,                    // Lower limit (x-direction)
               double b,                    // Upper limit (x-direction)
               double c(double),            // Lower limit (y-direction)
               double d(double),            // Upper limit (y-direction)
               double f(double x,double y), // Function to integrate
               int N,                       // Number of evaluations to make
               double* result,              // Pointer to hold the result
               double* error) {             // Pointer to hold the error

  // Use root of N as we are integrating on two directions
  int N_root = (int)sqrt(N);
  // Given an x, integration from c(x) to d(x) of f(x,y) wrt. y
  double F(double* x) {
    double result, error;
    double fx(double*y) {return f(*x,*y);}
    double cx = c(*x), dx = d(*x);
    plainmc(1,&cx,&dx,fx,N_root,&result,&error);
    return result;
  }

  plainmc(1,&a,&b,F,N_root,result,error);
}
