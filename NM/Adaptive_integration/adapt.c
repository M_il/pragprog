#include<math.h>
#include<assert.h>
#include<stdio.h>

double adapt24(
  double f(double),double a, double b,
  double acc, double eps, double f2, double f3, int nrec)
{
	assert(nrec<1000000); // Limit number of rects used for integration
  // Calculate
	double f1 = f(a + (b-a)/6),
         f4 = f(a + 5*(b-a)/6);
	double Q = (2*f1+f2+f3+2*f4) * (b-a)/6,
         q = (f1+f2+f3+f4) * (b-a)/4;
	double tolerance = acc+eps*fabs(Q),
         error     = fabs(Q-q)/2;
  // If the error is within the tolerence, then return the result
  // otherwise subdivide the region into two, and add their integrals
	if(error < tolerance) {
    return Q;
  }
	else {
    // Divide the interval, reduce the abs. err. by sqrt(2)
    // and carry on the number of rects used
		double Q1 = adapt24(f,a,(a+b)/2,acc/sqrt(2.),eps,f1,f2,nrec+1),
		       Q2 = adapt24(f,(a+b)/2,b,acc/sqrt(2.),eps,f3,f4,nrec+1);
		return Q1+Q2;
	}
}

// Adaptive quadrature using subdivition and high and low order rules (with open quadratures)
// There use a divition into {1/6,2/6,4/6,5/6} and {1/4,1/4,1/4,1/4}, respectively.
// This way the first two points may be used for the first half of the divition,
// and the last two point the last part, when resursively calling.
// The function values at the centre two points are generated here (parsed on via recursion),
// while the outer two points are generated in the algorithm.
double adapt(
  double f(double),double a,double b, // Integrand and limits
	double acc,double eps) // absolute and relative error limits
{
  double (*g)(double);
  // Identify infinete limits and transform accordingly
  if (isinf(a)) {
    assert(a < 0 && "Bad limit.");
    if (isinf(b)) {
      assert(b > 0 && "Bad limit.");
      a = -1; b = 1;
      double h(double t) { return f(t/(1-t*t))*(1+t*t)/((1-t*t)*(1-t*t)); } // Eq. 57
      // a = 0; b = 1;
      // double h(double t) { return f((1-t)/t) + f(-(1-t)/t)/(t*t); } // Eq. 58
      g = h;
    } else {
      // Eq. 61
      double h(double t) { return f(b + t/(1+t))/((1+t)*(1+t)); }
      g = h;
      a = -1; b = 0;
    }
  } else {
    if (isinf(b)) {
      assert(b > 0 && "Bad limit.");

      // double h(double t) { return f(a + (1-t)/t)/(t*t); } // Eq. 60
      double h(double t) { return f(a + t/(1-t))/((1-t)*(1-t)); } // Eq. 59
      g = h;
      a = 0; b = 1;
    } else {
      // Identity
      g = f;
    }
    assert("Bad limits.");
  }
	double f2 = g(a+2*(b-a)/6),
         f3 = g(a+4*(b-a)/6);
	int nrec = 0;
	return adapt24(g,a,b,acc,eps,f2,f3,nrec);
}

// Clenshaw Curtis transfrom as described in the notes
double clenshaw_curtis(double f(double),double a,double b,double acc,double eps){
	double g(double t){return f( (a+b)/2+(a-b)/2*cos(t) )*sin(t)*(b-a)/2;}
	return adapt(g,0,M_PI,acc,eps);
}
