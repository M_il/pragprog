#include<math.h>
#include<assert.h>
#include<stdio.h>
#include <gsl/gsl_integration.h>

double adapt(double f(double),double a,double b,double acc,double eps);
double clenshaw_curtis(double f(double),double a,double b,double acc,double eps);

int main() {
	int calls=0;
	double a=0,b=1,acc=1e-4,eps=1e-4;
	double s(double x){calls++; return sqrt(x);}; //nested function
	calls=0;
	double Q=adapt(s,a,b,acc,eps);
	double exact=2./3;
	printf("open4: integrating sqrt(x) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	calls=0;
	Q=clenshaw_curtis(s,a,b,acc,eps);
	exact=2./3;
	printf("\nclenshaw_curtis: integrating sqrt(x) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));


	calls=0;
	a=0,b=1,acc=1e-4,eps=1e-4;
	double f(double x){calls++; return 1/sqrt(x);}; //nested function
	calls=0;
	Q=adapt(f,a,b,acc,eps);
	exact=2;
	printf("\nopen4: integrating 1/sqrt(x) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	calls=0;
	Q=clenshaw_curtis(f,a,b,acc,eps);
	exact=2;
	printf("\nclenshaw_curtis: integrating 1/sqrt(x) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	a=0,b=1,acc=0.001,eps=0.001;
	double f2(double x){calls++; return log(x)/sqrt(x);}; //nested function
	calls=0;
	Q=adapt(f2,a,b,acc,eps);
	exact=-4;
	printf("\nopen4: integrating log(x)/sqrt(x) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	calls=0;
	Q=clenshaw_curtis(f2,a,b,acc,eps);
	exact=-4;
	printf("\nclenshaw_curtis: integrating log(x)/sqrt(x) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

  printf("Note: There is improvement in clenshaw_curtis over open4\n"
         "in functions with divergances, but also in integrating sqrt(x)\n");

  a=0,b=1,acc=0.001,eps=0.001;
  double f3(double x){calls++; return 4*sqrt(1-(1-x)*(1-x));}; //nested function
  calls=0;
  Q=adapt(f3,a,b,acc,eps);
  exact=M_PI;
  printf("\nopen4: integrating 4*sqrt(1-(1-x)^2) from %g to %g\n",a,b);
  printf("acc=%g eps=%g\n",acc,eps);
  printf("              Q = %g\n",Q);
  printf("          exact = %g\n",exact);
  printf("          calls = %d\n",calls);
  printf("estimated error = %g\n",acc+fabs(Q)*eps);
  printf("   actual error = %g\n",fabs(Q-exact));

  // Compare with GSL
  gsl_integration_workspace * w = gsl_integration_workspace_alloc(1000);
  double result, error;
  gsl_function F;
  F.params = NULL;

  a=0,b=INFINITY,acc=0.0001,eps=0.0001;
  double f4(double x){calls++; return sqrt(x)*exp(-x);}; //nested function
  double F4(double x, void *params) { return f4(x); }
  F.function = &F4;
  gsl_integration_qagiu(&F, a, acc, eps, 1000, w, &result, &error);

  calls=0;
  Q=adapt(f4,a,b,acc,eps);
  exact=sqrt(M_PI)/2;
  printf("\nopen4+GSL: integrating sqrt(x)*exp(-x) from %g to %g\n",a,b);
  printf("acc=%g eps=%g\n",acc,eps);
  printf("                 Q = %g\n",Q);
  printf("          GSL qags = %g\n",result);
  printf("             exact = %g\n",exact);
  printf("             calls = %d\n",calls);
  printf("   estimated error = %g\n",acc+fabs(Q)*eps);
  printf("         GSL error = %g\n",error);
  printf("      actual error = %g\n",fabs(Q-exact));
  printf("actual error (GSL) = %g\n",fabs(result-exact));

  a=-INFINITY,b=INFINITY,acc=0.0001,eps=0.0001;
  double f5(double x){calls++; return exp(-x*x);}; //nested function
  double F5(double x, void *params) { return f5(x); }
  F.function = &F5;
  gsl_integration_qagi(&F, acc, eps, 1000, w, &result, &error);

  calls=0;
  Q=adapt(f5,a,b,acc,eps);
  exact=sqrt(M_PI);
  printf("\nopen4+GSL: integrating exp(-x^2) from %g to %g\n",a,b);
  printf("acc=%g eps=%g\n",acc,eps);
  printf("                 Q = %g\n",Q);
  printf("          GSL qags = %g\n",result);
  printf("             exact = %g\n",exact);
  printf("             calls = %d\n",calls);
  printf("   estimated error = %g\n",acc+fabs(Q)*eps);
  printf("         GSL error = %g\n",error);
  printf("      actual error = %g\n",fabs(Q-exact));
  printf("actual error (GSL) = %g\n",fabs(result-exact));

  return 0;
}
