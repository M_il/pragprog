CFLAGS = -Wall -std=gnu11 -fopenmp -Wno-comment
LDFLAGS += -lgomp
# To use OpenMP
OS := $(shell uname)
ifeq ($(OS),Darwin) # gcc is clang on macOS so we using gcc-8 (real gcc)
    CC = gcc-8
else
    CC = gcc
endif

.PHONY: all
all: out_plainmc_test.txt plot_plainmc_error_test.svg

# Exersice A
out_plainmc_test.txt: plainmc_test plainmc_test_serial
	echo "------------- SERIAL METHOD -------------" > $@
	start_ms=$$(ruby -e 'puts (Time.now.to_f * 1000).to_i'); \
	./plainmc_test_serial >> $@; \
	end_ms=$$(ruby -e 'puts (Time.now.to_f * 1000).to_i'); \
	elapsed_ms=$$((end_ms - start_ms)); \
	echo "---------- TIME ELAPSED: "$$elapsed_ms" ms ----------" >> $@
	echo "" >> $@
	echo "------------- PARALLEL METHOD -------------" >> $@
	start_ms=$$(ruby -e 'puts (Time.now.to_f * 1000).to_i'); \
	./plainmc_test >> $@; \
	end_ms=$$(ruby -e 'puts (Time.now.to_f * 1000).to_i'); \
	elapsed_ms=$$((end_ms - start_ms)); \
	echo "---------- TIME ELAPSED: "$$elapsed_ms" ms ----------" >> $@;

plainmc_test: plainmc.o
	$(CC) $(CFLAGS) ../Monte_Carlo_integration/$@.c $< -o $@

plainmc_test_serial: CFLAGS:=$(filter-out -fopenmp,$(CFLAGS)) -Wno-unknown-pragmas
plainmc_test_serial: plainmc_serial.o
	$(CC) $(CFLAGS) ../Monte_Carlo_integration/plainmc_test.c $< -o $@

plainmc_serial.o: CFLAGS:=$(filter-out -fopenmp,$(CFLAGS)) -Wno-unknown-pragmas
plainmc_serial.o: plainmc.c
	$(CC) $(CFLAGS) -c $< -o $@

# Exersice B
plot_plainmc_error_test.svg: ../Monte_Carlo_integration/plainmc_error_test.gpi plainmc_error_test.data
	gnuplot -e "data_file='plainmc_error_test.data'" $< > $@

plainmc_error_test.data: plainmc_error_test
	./$< > $@

plainmc_error_test: plainmc.o
	$(CC) $(CFLAGS) ../Monte_Carlo_integration/$@.c ../Monte_Carlo_integration/$< -o $@

.PHONY: clean
clean: Makefile
	$(RM) plainmc_test *.txt *.svg *.o *.data
