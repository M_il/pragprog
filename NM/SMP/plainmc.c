#include <math.h>
#include <stdlib.h>
#include <omp.h>
unsigned int rand_seed = 0;
#define RND ((double)rand_r(&rand_seed)/RAND_MAX)

// Choses a random x vector within the aux. rectangular volume, V,
// enclosing the integration region befined by the limits of the integration.
void random_x(int dim, double *a, double *b, double *x) {
  for (int i=0;i<dim; i++)
    x[i] = a[i] + RND*(b[i]-a[i]);
}

void plainmc(int dim,               // Dimentionality of problem
             double *a,             // Lower limits of integration
             double *b,             // Upper limits of integration
             double f(double *x),   // Function to integrate
             int N,                 // Number of evaluations to make
             double* result,        // Pointer to hold the result
             double* error) {       // Pointer to hold the error

  // Volume of the aux. integration space
  double V = 1;
  for (int i=0;i<dim;i++)
    V*=b[i]-a[i];

  double  sum=0, sum_2 = 0,
          sum2=0, sum2_2 = 0,
          fx,
          x[dim];

  // For all number of eval points N, choose a random x in V
  //  calculate the sum of f(x) in eq. 2
  //  calculate the sum (sum2) of (f(x))^2 to be usid for sigma error estimate
  // int i;
  // #pragma omp parallel for \
  //  shared(dim,a,b,x,fx,f,sum,sum2) private(i) \
  //  schedule(static,1000)
  // for (i=0;i<N; i++) {
  //   random_x(dim,a,b,x);
  //   fx=f(x);
  //   sum+=fx;
  //   sum2+=fx*fx;
  // }

  int i;
  #pragma omp parallel shared(dim,a,b,f,N) private(i,x,fx)
  {
    #pragma omp sections
    {
      #pragma omp section
      {
        for (i=0;i<N/2; i++) {
          random_x(dim,a,b,x);
          fx=f(x);
          sum+=fx;
          sum2+=fx*fx;
        }
      }
      #pragma omp section
      {
        for (i=N/2;i<N; i++) {
          random_x(dim,a,b,x);
          fx=f(x);
          sum_2+=fx;
          sum2_2+=fx*fx;
        }
      }
    }
  }
  sum += sum_2;
  sum2 += sum2_2;

  // Calculate the results according to eq. 2,
  // the error according to eq. 3 and 4
  double  avr = sum/N,
          var = sum2/N-avr*avr;
          *result = avr*V;
          *error = sqrt(var/N)*V;
}
