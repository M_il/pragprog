#ifndef LSFIT_H
#define LSFIT_H

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>
#include <assert.h>
#include <qr.h>

void lsfit(gsl_vector *x,                // x-values
           gsl_vector *y,                // y-values
           gsl_vector *dy,               // Errors in y
           int nf,                       // Number of fitting functions
           double f(int k, double x),    // Proxy for k'th fitting functions
           gsl_vector *c,                // Result of fitting coefficients
           gsl_matrix *S);               // Covariance matrix

double lsfit_eval(double x,                   // x-value
                  gsl_vector *c,              // Fitting coefficients
                  int nf,                     // Number of fitting functions
                  double f(int k, double x)); // Proxy for k'th fitting functions

void calc_delta_c(gsl_matrix *S, gsl_vector *delta_c);
#endif
