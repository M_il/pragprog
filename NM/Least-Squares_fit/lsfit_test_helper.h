#ifndef LSFIT_TEST_HELPER_H
#define LSFIT_TEST_HELPER_H
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <math.h>
#include "lsfit.h"
#define RND ((double)rand()/RAND_MAX)

gsl_vector *alloc_vector_from_array(double *a, int n);

// Generates random data
void random_data_with_basis(int nf,
                            double f(int k, double x),
                            double *c,
                            gsl_vector *x,
                            gsl_vector *y,
                            gsl_vector *dy);

void print_x_y_dy(gsl_vector *x, gsl_vector *y, gsl_vector *dy);

void print_fit(gsl_vector *x,
               gsl_vector *c,
               gsl_matrix *S,
               int n_functions,
               double (*fit_functions)(int k, double x));
#endif
