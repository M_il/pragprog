#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <math.h>
#include "lsfit.h"
#include "lsfit_test_helper.h"

double x_array[]  = {0.10, 1.33, 2.55, 3.78, 5.00,6.22, 7.45, 8.68, 9.90};
double y_array[]  = {-15.3,0.32, 2.45, 2.75, 2.27,1.35, 0.157,-1.23,-2.75};
double dy_array[] = {1.04, 0.594,0.983,0.998,1.11,0.398,0.535,0.968,0.478};
int n_datapoints = (int)(sizeof(x_array)/sizeof(double));

int n_functions = 3;
double fit_functions(int k, double x) {
  switch (k) {
    case 0: return log(x);
    case 1: return 1;
    case 2: return x;
    default:
      fprintf(stderr, "No such function index %i.\n", k);
      return NAN;
  }
}

int n_functions_B1 = 2;
double fit_fucntions_B1(int k, double x) {
  switch (k) {
    case 0: return sin(x/2);
    case 1: return 1;
    default:
      fprintf(stderr, "No such function index %i.\n", k);
      return NAN;
  }
}

int n_functions_B2 = 3;
double fit_fucntions_B2(int k, double x) {
  switch (k) {
    case 0: return 1/x;
    case 1: return x;
    case 2: return 1;
    default:
      fprintf(stderr, "No such function index %i.\n", k);
      return NAN;
  }
}

int main(int argc, char const *argv[]) {
  gsl_vector *x  = alloc_vector_from_array(x_array, n_datapoints);
  gsl_vector *y  = alloc_vector_from_array(y_array, n_datapoints);
  gsl_vector *dy = alloc_vector_from_array(dy_array, n_datapoints);

  gsl_vector *c = gsl_vector_alloc(n_functions);
  gsl_matrix *S = gsl_matrix_alloc(n_functions,n_functions);

  // Fit data to functions
  lsfit(x,y,dy,n_functions,fit_functions,c,S);

  // Print data points
  printf("# Exersice A\n");
  printf("# x y dy\n");
  print_x_y_dy(x,y,dy);
  printf("\n\n");

  // Print function values in between datapoints
  printf("# Fitfunction: %g log(x) + %g x + %g\n", gsl_vector_get(c,0), gsl_vector_get(c,2), gsl_vector_get(c,1));
  print_fit(x,c,S,n_functions,fit_functions);
  printf("\n\n");

  // Free them and reuse the variables
  gsl_vector_free(c);
  gsl_matrix_free(S);
  c = gsl_vector_alloc(n_functions_B1);
  S = gsl_matrix_alloc(n_functions_B1, n_functions_B1);

  // Exersice B - exmaple 1
  printf("# Exersice B - example 1\n");
  printf("# x y dy\n");
  // Generate random data using a basis of functions and their coeff.
  double c_init[] = {20,30.5};
  random_data_with_basis(n_functions_B1, fit_fucntions_B1, c_init, x, y, dy);
  print_x_y_dy(x,y,dy);
  printf("\n\n");

  // Fit it
  lsfit(x,y,dy,n_functions_B1, fit_fucntions_B1,c,S);
  printf("# Fitfunction 2: %g sin(x) + %g\n", gsl_vector_get(c,0), gsl_vector_get(c,1));
  print_fit(x,c,S,n_functions_B1, fit_fucntions_B1);
  printf("\n\n");

  // Free them and reuse the variables
  gsl_vector_free(c);
  gsl_matrix_free(S);
  c = gsl_vector_alloc(n_functions_B2);
  S = gsl_matrix_alloc(n_functions_B2, n_functions_B2);

  // Exersice B - exmaple 2
  printf("# Exersice B - example 2\n");
  printf("# x y dy\n");
  // Generate random data using a basis of functions and their coeff.
  double c_init2[] = {14.3,7.5,10};
  random_data_with_basis(n_functions_B2, fit_fucntions_B2, c_init2, x, y, dy);
  print_x_y_dy(x,y,dy);
  printf("\n\n");

  // Fit it
  lsfit(x,y,dy,n_functions_B2, fit_fucntions_B2,c,S);
  printf("# Fitfunction 2: %g/x + %gx + %g\n", gsl_vector_get(c,0), gsl_vector_get(c,1), gsl_vector_get(c,2));
  print_fit(x,c,S,n_functions_B2, fit_fucntions_B2);
  printf("\n\n");

  gsl_vector_free(x);gsl_vector_free(y);gsl_vector_free(dy);
  gsl_vector_free(c);gsl_matrix_free(S);

  return 0;
}
