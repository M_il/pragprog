#import "lsfit.h"

void lsfit(gsl_vector *x,                // x-values
           gsl_vector *y,                // y-values
           gsl_vector *dy,               // Errors in y
           int nf,                       // Number of fitting functions
           double f(int k, double x),    // Proxy for k'th fitting functions
           gsl_vector *c,                // Result of fitting coefficients
           gsl_matrix *S)                // Covariance matrix
{
  int n = x->size, m = nf;
  assert(c->size == m && S->size1==m && S->size2 == m);

  // Creating matrices A and b according to eq. 7
  gsl_matrix *A = gsl_matrix_alloc(n,m), *R = gsl_matrix_alloc(m,m);
  gsl_vector *b = gsl_vector_alloc(n);
  for (int i = 0; i < n; i++) {
    double xi = gsl_vector_get(x,i), dyi = gsl_vector_get(dy,i);
    gsl_vector_set(b, i, gsl_vector_get(y,i)/dyi);
    for (int k = 0; k < m; k++) {
      gsl_matrix_set(A, i, k, f(k,xi)/dyi);
    }
  }

  // Constructing QR=A, and solve Rc = Q^T b (eq. 4) to get c
  qr_gs_decomp(A,R);
  gsl_matrix *Q = A;
  qr_gs_solve(Q,R,b,c);

  // Calculating covariance matrix, S, using R according to eq. 14
  // It's used that R is upper triangular using gsl_blas_dtrmm: R^T*R
  gsl_matrix *R_cpy = gsl_matrix_alloc(m,m);
  gsl_matrix_memcpy(R_cpy, R);
  gsl_blas_dtrmm(CblasLeft, CblasUpper, CblasTrans, CblasNonUnit, 1, R_cpy, R);
  gsl_matrix *RT_R_inv = R;
  // (R^T*R)^-1
  gsl_matrix *R__RT_R_inv = gsl_matrix_alloc(m,m);
  qr_gs_decomp(RT_R_inv, R__RT_R_inv);
  gsl_matrix *Q__RT_R_inv = RT_R_inv;
  qr_gs_inverse(Q__RT_R_inv, R__RT_R_inv, S); // S <- (R^T*R)^-1

  gsl_matrix_free(A);gsl_matrix_free(R);gsl_vector_free(b);
  gsl_matrix_free(R_cpy); gsl_matrix_free(R__RT_R_inv);
}

double lsfit_eval(double x,                   // x-value
                  gsl_vector *c,              // Fitting coefficients
                  int nf,                     // Number of fitting functions
                  double f(int k, double x))  // Proxy for k'th fitting functions
{
  double sum = 0;
  for (int k = 0; k < nf; k++) {
    sum += gsl_vector_get(c,k)*f(k,x);
  }
  return sum;
}

// Calculates delta c from covariance matrix
void calc_delta_c(gsl_matrix *S, gsl_vector *delta_c) {
  for (int k = 0; k < S->size1; k++) {
    double sqrt_S_kk = sqrt(gsl_matrix_get(S,k,k));
    gsl_vector_set(delta_c,k,sqrt_S_kk);
  }
}
