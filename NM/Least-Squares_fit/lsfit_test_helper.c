#include "lsfit_test_helper.h"

gsl_vector *alloc_vector_from_array(double *a, int n) {
  gsl_vector *v = gsl_vector_alloc(n);
  while (n-- != 0) {
    gsl_vector_set(v, n, a[n]);
  }
  return v;
}

// Generates random data
void random_data_with_basis(int nf,
                            double f(int k, double x),
                            double *c,
                            gsl_vector *x,
                            gsl_vector *y,
                            gsl_vector *dy) {
  for (int i = 0; i < x->size; i++) {
    gsl_vector_set(y,i,0);
    gsl_vector_set(dy,i,(5*RND + 10)); // Might not be optimal erros
    double sum = 0;
    for (int k = 0; k < nf; k++) {
      sum += c[k]*f(k,gsl_vector_get(x,i));
    }
    gsl_vector_set(y,i, sum + 6*RND);
  }
}

void print_x_y_dy(gsl_vector *x, gsl_vector *y, gsl_vector *dy) {
  for (int i = 0; i < x->size; i++) {
    printf("%g %g %g\n", gsl_vector_get(x,i), gsl_vector_get(y,i), gsl_vector_get(dy,i));
  }
}

void print_fit(gsl_vector *x,
               gsl_vector *c,
               gsl_matrix *S,
               int n_functions,
               double (*fit_functions)(int k, double x))
{
  // Claculate the ∆c for the function confidence fits
  gsl_vector *delta_c = gsl_vector_alloc(c->size);
  calc_delta_c(S,delta_c);

  // Save c+∆c and c-∆c reusing pointer for ∆c
  // Calculated according to eq. 15
  gsl_vector *c_plus_delta_c = gsl_vector_alloc(c->size);
  gsl_vector_memcpy(c_plus_delta_c, c);
  gsl_vector_add(c_plus_delta_c, delta_c);
  gsl_vector *c_minus_delta_c = delta_c;
  gsl_vector_scale(c_minus_delta_c,-1);
  gsl_vector_add(c_minus_delta_c, c);

  printf("# x f(x) f_minus(x) f_plus(x)\n");
  int n_datapoints = x->size;
  double x_min = gsl_vector_get(x,0), x_max = gsl_vector_get(x,n_datapoints-1);
  double x_increment = (x_max - x_min)/50;
  for (double x = x_min; x < x_max; x+=x_increment) {
    double fx = lsfit_eval(x,c,n_functions,fit_functions);
    double fx_minus = lsfit_eval(x,c_minus_delta_c,n_functions,fit_functions);
    double fx_plus = lsfit_eval(x,c_plus_delta_c,n_functions,fit_functions);
    printf("%g %g %g %g\n", x, fx, fx_minus, fx_plus);
  }

  gsl_vector_free(c_plus_delta_c);
  gsl_vector_free(c_minus_delta_c);
}
