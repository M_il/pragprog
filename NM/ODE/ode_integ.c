#include "ode_integ.h"

double ode_integ(double f(double), double a, double b, double acc, double eps) {
  void ode_f(int n,double x,double*y,double*dydx) { dydx[0] = f(x); }

  int max = 1e3;

  double*  xlist = (double*)calloc(max,sizeof(double));
	double** ylist = (double**)calloc(max,sizeof(double*));
	for (int i=0;i<max;i++) {
    ylist[i] = (double*)calloc(1,sizeof(double));
  }

  // Solve the system y'=f(x), y(a)=0 : I = y(b)
  double h = 0.1;
  xlist[0] = a; ylist[0][0] = 0;
  int k = ode_driver(ode_f,1,xlist,ylist,b,h,acc,eps,max,rkstep12);

  // Save the result
  double result = ylist[k-1][0];

  free(xlist);
  for (int i=0;i<max;i++) { free(ylist[i]); }
  free(ylist);

  return result;
}
