#ifndef RKSTEP12_H
#define RKSTEP12_H

void rkstep12(
  void f(int n, double x, double* yx, double* dydx),
  int n, double x, double* yx,
  double h, double* yh, double* dy);

#endif
