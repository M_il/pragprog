#ifndef ODE_DRIVER_H
#define ODE_DRIVER_H

#include <math.h>
#include <stdlib.h>
#include <stdio.h>

int ode_driver(void f(int n,double x,double*y,double*dydx), // f = y' = dy/dx
               int n,          // numer of equation
               double*xlist,   // x values throughout integration
               double**ylist,  // y-vector values throughout integration
               double b,       // endpoint of integration
               double h,       // step size
               double acc,     // absolute acculary
               double eps,     // relative accuracy
               int max,        // max itterations
               void (*ode_stepper)(   // stepper function to use
                 void (*f)(int n,double x,double*y,double*dydx),
                 int n, double x, double* y, double h, double* yh, double* dy));

#endif
