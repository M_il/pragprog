#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ode_driver.h"
#include "rkstep12.h"

// Harmosic oscilator: dy^2/dx^2 = -y
void f(int n, double x, double* y, double* dydx) {
	dydx[0] = y[1];
	dydx[1] = -y[0];
	return;
}

double C = 2, mu = 0.5;
void brachistochrone(int n, double t, double* v, double* dydt) {
  // double x = v[0];
  // double y = v[1];
	// dydt[0] = (C*mu - sqrt((y - mu*x)*(C*mu*mu + C + mu*x - y))) /
  //           (y - mu*(C*mu + x));
  dydt[0] = 0.5*(1-cos(t));
  dydt[1] = -0.5*sin(t);
	return;
}

void solve_brachistochrone(double* xlist, double** ylist) {
  int n=2;
	int max=1000;
	double a=0, b=M_PI, h=0.1, acc=0.01, eps=0.01;
	xlist[0]=a; ylist[0][0]=0; ylist[0][1]=1;
	int k = ode_driver(brachistochrone,n,xlist,ylist,b,h,acc,eps,max,rkstep12);
  printf("# brachistochrone\n");
	if (k<0) {
    printf("max steps reached in ode_driver\n");
  }
	printf("# ODE solution\n");
	for (int i=0;i<k;i++) {
    printf("%g %g\n",ylist[i][0],ylist[i][1]);
  }
  printf("\n\n");
	printf("# exact solution\n");
	for (double t=0;t<1e3*M_PI;t+=M_PI/100) {
    double k2 = 1/(2*9.8*C*C);
    double x = 0.5*k2 * ((t-sin(t)) + mu*(1-cos(t))),
           y = -0.5*k2 * ((1-cos(t)) + mu*(t+sin(t))) + 1;
    printf("%g %g\n",x,y);
  }
}

// dy/dx = -(t+1)*y;
void f2(int n, double t, double* y, double* dydx) {
  dydx[0] = -(t+1)*y[0];
	return;
}

void solve_f2(double* xlist, double** ylist) {
  int n=1;
	int max=1000;
	double a=1, b=3, h=0.1, acc=0.01, eps=0.01;
	xlist[0]=a; ylist[0][0]=1;
	int k = ode_driver(f2,n,xlist,ylist,b,h,acc,eps,max,rkstep12);
  printf("# 2nd ODE\n");
	if (k<0) {
    printf("max steps reached in ode_driver\n");
  }
	printf("# ODE solution\n");
	for (int i=0;i<k;i++) {
    printf("%g %g\n",xlist[i],ylist[i][0]);
  }
  printf("\n\n");
	printf("# exact solution\n");
  for (int i=0;i<k;i++) {
    double x = xlist[i];
    printf("%g %g\n",x,exp(1.5)*exp(-(x*x/2+x)));
  }
}

int main() {
	int n=2;
	int max=1000;
	double*  xlist = (double*)calloc(max,sizeof(double));
	double** ylist = (double**)calloc(max,sizeof(double*));
	for (int i=0;i<max;i++) {
    ylist[i] = (double*)calloc(n,sizeof(double));
  }
	double a=0, b=8*M_PI, h=0.1, acc=0.01, eps=0.01;
	xlist[0]=a; ylist[0][0]=0; ylist[0][1]=1;
	int k = ode_driver(f,n,xlist,ylist,b,h,acc,eps,max,rkstep12);
	if (k<0) {
    printf("max steps reached in ode_driver\n");
  }
	printf("# ODE solution\n");
	for (int i=0;i<k;i++) {
    printf("%g %g\n",xlist[i],ylist[i][0]);
  }
  printf("\n\n");
	printf("# exact solution\n");
	for (int i=0;i<k;i++) {
    printf("%g %g\n",xlist[i],sin(xlist[i]));
  }
  printf("\n\n");

  solve_f2(xlist, ylist);

  free(xlist);
  for (int i=0;i<max;i++) { free(ylist[i]); }
  free(ylist);
	return 0;
}
