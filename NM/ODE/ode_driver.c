#include "ode_driver.h"

// Solves a ode system
int ode_driver(void f(int n,double x,double*y,double*dydx), // f = y' = dy/dx
               int n,          // numer of equation
               double*xlist,   // x values throughout integration
               double**ylist,  // y-vector values throughout integration
               double b,       // endpoint of integration
               double h,       // step size
               double acc,     // absolute acculary
               double eps,     // relative accuracy
               int max,        // max itterations
               void (*ode_stepper)(   // stepper function to use
                 void (*f)(int n,double x,double*y,double*dydx),
                 int n, double x, double* y, double h, double* yh, double* dy))
{
  int i,k=0;
  double x,*y,s,err,normy,tol,a=xlist[0],yh[n],dy[n];
  // Advance steppesr while not reached the endponit
  while(xlist[k]<b){
    x = xlist[k], y = ylist[k]; // Get current x and y
    if(x+h>b) { // Don't steps past end
      h=b-x;
    }
    ode_stepper(f,n,x,y,h,yh,dy); // Get derivatives
    // Calculate erros erros and comp. with relative and absolute requered
    s=0; for(i=0;i<n;i++) s+=dy[i]*dy[i]; err  =sqrt(s);
    s=0; for(i=0;i<n;i++) s+=yh[i]*yh[i]; normy=sqrt(s);
    tol=(normy*eps+acc)*sqrt(h/(b-a));

    // Accept steps if within tolerence
    if(err<tol){
      k++;
      if(k>max-1) { // Return -k if it reaches max steps
        return -k;
      }
      xlist[k] = x+h;     // Update x and y-vector
      for(i=0;i<n;i++) {  //
        ylist[k][i]=yh[i];
      }
    }
    // Reduce steps size if error > 0 otherwise double it
    if(err>0) {
      h*=pow(tol/err,0.25)*0.95;
    } else {
      h*=2;
    }
  }
  // Return the number of entries in xlist/ylist
  return k+1;
}
