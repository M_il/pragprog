#include "ode_integ.h"
#include <math.h>

int main(int argc, char const *argv[]) {
  double acc = 1e-3, eps = 1e-3;
  double a = 2*M_PI, b = 3*M_PI;
  double exact = 2;

  printf("Using acc = %g, eps 0 %g\n", acc, eps);
  printf("Integral of sin(x) from 2*pi to 4*pi\n");
  double result = ode_integ(&sin,a,b,acc,eps);
  printf("Result = %g\n", result);
  printf("Exact = %g\n", exact);
  printf("Error = %g\n\n", exact - result);

  printf("Using acc = %g, eps 0 %g\n", acc, eps);
  printf("Integral of cos(x) from 2*pi to 4*pi\n");
  exact = 0;
  result = ode_integ(&cos,a,b,acc,eps);
  printf("Result = %g\n", result);
  printf("Exact = %g\n", exact);
  printf("Error = %g\n\n", exact - result);

  printf("Using acc = %g, eps 0 %g\n", acc, eps);
  printf("Integral of tanh(x) from 0 to pi\n");
  a = 0; b = M_PI; exact = log(cosh(M_PI));
  result = ode_integ(&tanh,a,b,acc,eps);
  printf("Result = %g\n", result);
  printf("Exact = %g\n", exact);
  printf("Error = %g\n\n", exact - result);
  return 0;
}
