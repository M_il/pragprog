#include "rkstep12.h"

// Embedded midpoint-Euler method with error estimate
void rkstep12(
  // f = y' = dy/dx
  void f(int n, double x, double* yx, double* dydx),
  // n: numer of equation, x: start x, yx: y(x)
  int n, double x, double* yx,
  // h: steps size, yh: y(x)+h*k, dy: error estimate
  double h, double* yh, double* dy) {
  double k0[n],yt[n],k_half[n];
  int i;

  // These are according to eq. 10
  // Calc. k0
  f(n,x,yx,k0);
  // Calc. k_half
  for (i=0;i<n;i++) {
    yt[i]=yx[i] + k0[i]*h/2;
  }
  f(n,x+h/2,yt,k_half);
  // Calc. the update (eq. 12)
  for (i=0;i<n;i++) {
    yh[i] = yx[i] + k_half[i]*h;
  }
  // Calc. error estimate
  for (i=0;i<n;i++) {
    dy[i] = (k0[i]-k_half[i])*h/2;
  }
}
