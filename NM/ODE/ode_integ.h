#ifndef ODE_INTEG_H
#define ODE_INTEG_H

#include "ode_driver.h"
#include "rkstep12.h"
#include <stdio.h>

double ode_integ(double f(double), double a, double b, double acc, double eps);

#endif
