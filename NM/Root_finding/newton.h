#ifndef NEWTON_H
#define NEWTON_H
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <stdbool.h>
#include <qr.h>
#include <givens.h>

int newton(int f(const gsl_vector* x, gsl_vector* fx),   // Function to find root of
            gsl_vector* x,                               // Start point and result of root finding
            double dx,                                   // Numerical Jacobian step size
            double eps);                                 // Tolerance

int newton_with_jacobian(
	int f(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J),  // Function to find root of
	gsl_vector* x,                                              // Start point and result of root finding
	double eps);                                                // Tolerance

#endif
