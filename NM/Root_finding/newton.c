#include "newton.h"
#define MIN_LAMB 1.0/64

void print_matrix(gsl_matrix *A){
	for(int r=0;r<A->size1;r++){
		for(int c=0;c<A->size2;c++) {
      fprintf(stderr,"%10.3f",gsl_matrix_get(A,r,c));
    }
		fprintf(stderr,"\n");
  }
}

void print_vector(char* s, const gsl_vector* v) {
  printf("%s",s);
  for (int i = 0; i < v->size; i++) {
    printf("%10.3g", gsl_vector_get(v,i));
  }
  printf("\n");
}

// Newton root-finding algorithm of function f
int newton(int f(const gsl_vector* x, gsl_vector* fx), // Function to find root of
            gsl_vector* x,                               // Start point and result of root finding
            double dx,                                   // Numerical Jacobian step size
            double eps)                                  // Tolerance
{
  int steps = 0;
  int n=x->size;                          // Dimension of problem
  gsl_matrix* J  = gsl_matrix_alloc(n,n); // Jacobian
  gsl_vector* fx = gsl_vector_alloc(n);   // f(x)
  gsl_vector* z  = gsl_vector_alloc(n);
  gsl_vector* fz = gsl_vector_alloc(n);
  gsl_vector* df = gsl_vector_alloc(n);
  gsl_vector* Dx = gsl_vector_alloc(n);
  do {
    steps++;
    f(x,fx);    // fx <- f(x)
    // Finding the Jacobian numerically
    for (int j=0; j<n; j++) {
      gsl_vector_set(x,j,gsl_vector_get(x,j)+dx); // xj += dy
      f(x,df);                                    // df = f(x)
      gsl_vector_sub(df,fx);                      // df = f(x+dx)-f(x)
      for (int i=0; i<n; i++) {
          gsl_matrix_set(J,i,j,gsl_vector_get(df,i)/dx); // J_j = df/dx (j'th column in J)
      }
      gsl_vector_set(x,j,gsl_vector_get(x,j)-dx); // xj -= dx
    }

    qr_dec(J);
    gsl_vector_memcpy(Dx,fx); // solving J∆x = -f(x) for ∆x
    qr_solve_inplace(J,Dx);   //
    gsl_vector_scale(Dx,-1);  // Remembering the sign

    double lamb = 1;
    do {
      gsl_vector_memcpy(z,x);     // z = x+λ∆x
      gsl_vector_add(z,Dx);       //
      f(z,fz);                    // fz = f(z)
      // fprintf(stderr,"z = %g,%g", gsl_vector_get(z,0), gsl_vector_get(z,1));
      // fprintf(stderr, "||fx|| = %g, ||fz|| = %g, lamb = %g\n", gsl_blas_dnrm2(fx), gsl_blas_dnrm2(fz), lamb);
      lamb *= 0.5;
      gsl_vector_scale(Dx,0.5);
    } while (gsl_blas_dnrm2(fz) > (1-lamb/2)*gsl_blas_dnrm2(fx) && lamb>MIN_LAMB);
    gsl_vector_memcpy(x,z);   // x <- x+λ∆x
    gsl_vector_memcpy(fx,fz); // f(x) = f(x+λ∆x)
    // Converged if ||∆x|| < dx or ||f(x)|| < ε
    // fprintf(stderr, "x = %g %g\n", gsl_vector_get(x,0), gsl_vector_get(x,1));
  } while (gsl_blas_dnrm2(Dx)>dx && gsl_blas_dnrm2(fx)>eps);
  gsl_matrix_free(J);
  gsl_vector_free(fx);
  gsl_vector_free(z);
  gsl_vector_free(fz);
  gsl_vector_free(df);
  gsl_vector_free(Dx);

  return steps;
}

// Newton root funding using a Jacobian from f
int newton_with_jacobian(
	int f(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J), // Function to find root of
	gsl_vector* x,                                              // Start point and result of root finding
	double eps)                                                 // Tolerance
{
  int steps = 0;
  int n=x->size;                          // Dimension of problem
  gsl_matrix* J  = gsl_matrix_alloc(n,n); // Jacobian
  gsl_vector* fx = gsl_vector_alloc(n);   // f(x)
  gsl_vector* z  = gsl_vector_alloc(n);
  gsl_vector* fz = gsl_vector_alloc(n);
  gsl_vector* df = gsl_vector_alloc(n);
  gsl_vector* Dx = gsl_vector_alloc(n);
  do {
    steps++;
    f(x,fx,J);    // fx <- f(x), get J

    qr_dec(J);
    gsl_vector_memcpy(Dx,fx); // solving J∆x = -f(x) for ∆x
    qr_solve_inplace(J,Dx);   //
    gsl_vector_scale(Dx,-1);  // Remembering the sign

    double lamb = 1;
    do {
      gsl_vector_memcpy(z,x);     // z = x+λ∆x
      gsl_vector_add(z,Dx);       //
      f(z,fz,NULL);               // fz = f(z)
      // fprintf(stderr,"z = %g,%g", gsl_vector_get(z,0), gsl_vector_get(z,1));
      // fprintf(stderr, "||fx|| = %g, ||fz|| = %g, lamb = %g\n", gsl_blas_dnrm2(fx), gsl_blas_dnrm2(fz), lamb);
      lamb *= 0.5;
      gsl_vector_scale(Dx,0.5);
    } while (gsl_blas_dnrm2(fz) > (1-lamb/2)*gsl_blas_dnrm2(fx) && lamb>MIN_LAMB);
    gsl_vector_memcpy(x,z);   // x <- x+λ∆x
    gsl_vector_memcpy(fx,fz); // f(x) = f(x+λ∆x)
    // Converged if ||∆x|| < dx or ||f(x)|| < ε
    // fprintf(stderr, "x = %g %g\n", gsl_vector_get(x,0), gsl_vector_get(x,1));
  } while (gsl_blas_dnrm2(fx)>eps);
  gsl_matrix_free(J);
  gsl_vector_free(fx);
  gsl_vector_free(z);
  gsl_vector_free(fz);
  gsl_vector_free(df);
  gsl_vector_free(Dx);

  return steps;
}
