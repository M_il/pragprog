#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>

int gsl_root_find(int f(const gsl_vector* x, gsl_vector* fx),
                  gsl_vector* x,
                  double eps)
{
  int f_gsl(const gsl_vector* x, void* params, gsl_vector* fx) {
    f(x,fx); return GSL_SUCCESS;
  }

  gsl_multiroot_fsolver *s;

  int status;
  int steps = 0;

  const size_t n = 2;
  gsl_multiroot_function F = {&f_gsl, n, NULL};

  s = gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_hybrids, 2);
  gsl_multiroot_fsolver_set(s, &F, x);

  do {
    steps++;
    status = gsl_multiroot_fsolver_iterate(s);

    if (status) // If it's stuck then break
      break;

    status = gsl_multiroot_test_residual(s->f, eps);
  } while (status == GSL_CONTINUE && steps < 1000);

  gsl_vector_memcpy(x,s->x);

  gsl_multiroot_fsolver_free(s);

  return steps;
}
