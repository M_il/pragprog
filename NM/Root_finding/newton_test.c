#include <gsl/gsl_vector.h>
#include <math.h>
#include "newton.h"
#define CONST_A 1e4

int f(const gsl_vector* _x, gsl_vector* fx) {
  static int calls = 0; if (_x != NULL) {calls++;} else {calls=0; return 0;};
  double x = gsl_vector_get(_x,0), y = gsl_vector_get(_x,1);
  double result = CONST_A*x*y - 1;
  double result2 = exp(-x) + exp(-y) - 1 - 1.0/CONST_A;
  gsl_vector_set(fx,0,result);
  gsl_vector_set(fx,1,result2);
  return calls;
}

int f_J(const gsl_vector* _x, gsl_vector* fx, gsl_matrix* J) {
  static int calls = 0; if (_x != NULL) {calls++;} else {calls=0; return 0;};
  double x = gsl_vector_get(_x,0), y = gsl_vector_get(_x,1);
  f(_x,fx);
  if (J != NULL) {
    double f1dx = CONST_A*y, f1dy = CONST_A*x,
           f2dx = -exp(-x), f2dy = -exp(-y);
    gsl_matrix_set(J,0,0,f1dx); gsl_matrix_set(J,0,1,f1dy);
    gsl_matrix_set(J,1,0,f2dx); gsl_matrix_set(J,1,1,f2dy);
  }
  return calls;
}

// Minimize f(x,y) = (1-x)^2 + 100(y-x^2)^2 using its gradient
// http://www.wolframalpha.com/input/?i=grad+(1-x)%5E2+%2B+100(y-x%5E2)%5E2
int rosenbrock(const gsl_vector* _x, gsl_vector* fx) {
  static int calls = 0; if (_x != NULL) {calls++;} else {calls=0; return 0;};
  double x = gsl_vector_get(_x,0), y = gsl_vector_get(_x,1);
  double grad_x = 2*(200*x*x*x - 200*x*y + x - 1),
         grad_y = 200*(y - x*x);
  gsl_vector_set(fx,0,grad_x);
  gsl_vector_set(fx,1,grad_y);
  return calls;
}

// http://www.wolframalpha.com/input/?i=jacobian+of+%7B2*(200*x*x*x+-+200*x*y+%2B+x+-+1),200*(y+-+x*x)%7D
int rosenbrock_J(const gsl_vector* _x, gsl_vector* fx, gsl_matrix* J) {
  static int calls = 0; if (_x != NULL) {calls++;} else {calls=0; return 0;};
  double x = gsl_vector_get(_x,0), y = gsl_vector_get(_x,1);
  rosenbrock(_x,fx);
  if (J != NULL) {
    double f1dx = 2*(600*x*x - 200*y + 1), f1dy = -400*x,
           f2dx = -400*x, f2dy = 200;
    gsl_matrix_set(J,0,0,f1dx); gsl_matrix_set(J,0,1,f1dy);
    gsl_matrix_set(J,1,0,f2dx); gsl_matrix_set(J,1,1,f2dy);
  }
  return calls;
}

// Minimize f(x,y) = (x^2+y-11)^2+(x+y^2-7)^2 using its gradient
// http://www.wolframalpha.com/input/?i=grad+(x%5E2%2By-11)%5E2%2B(x%2By%5E2-7)%5E2
int himmelblau(const gsl_vector* _x, gsl_vector* fx) {
  static int calls = 0; if (_x != NULL) {calls++;} else {calls=0; return 0;};
  double x = gsl_vector_get(_x,0), y = gsl_vector_get(_x,1);
  double grad_x = 2*(2*x*(x*x+y-11) + x+y*y-7),
         grad_y = 2*(x*x+2*y*(x+y*y-7)+y-11);
  gsl_vector_set(fx,0,grad_x);
  gsl_vector_set(fx,1,grad_y);
  return calls;
}

// http://www.wolframalpha.com/input/?i=jacobian+of+%7B2*(2*x*(x*x%2By-11)+%2B+x%2By*y-7),2*(x*x%2B2*y*(x%2By*y-7)%2By-11)%7D
int himmelblau_J(const gsl_vector* _x, gsl_vector* fx, gsl_matrix* J) {
  static int calls = 0; if (_x != NULL) {calls++;} else {calls=0; return 0;};
  double x = gsl_vector_get(_x,0), y = gsl_vector_get(_x,1);
  himmelblau(_x,fx);
  if (J != NULL) {
    double f1dx = 2*(4*x*x+2*(x*x+y-11)+1), f1dy = 2*(2*x+2*y),
           f2dx = 2*(2*x+2*y), f2dy = 2*(4*y*y+2*(y*y+x-7)+1);
    gsl_matrix_set(J,0,0,f1dx); gsl_matrix_set(J,0,1,f1dy);
    gsl_matrix_set(J,1,0,f2dx); gsl_matrix_set(J,1,1,f2dy);
  }
  return calls;
}

void print_vector(char* s, const gsl_vector* v);
int gsl_root_find(int f(const gsl_vector* x, gsl_vector* fx),
                  gsl_vector* x,
                  double eps);

int main(int argc, char const *argv[]) {
  // System of equations
  gsl_vector* x = gsl_vector_alloc(2);
  double dx = 1e-4, eps = 1e-4;
  gsl_vector *fx = gsl_vector_alloc(2);
  int steps, calls;

  // Linear system
  printf("--- Newton with simple back-tracking ---\n");
  printf("Using dx = %g and eps = %g\n", dx, eps);
  gsl_vector_set(x,0,5);
  gsl_vector_set(x,1,1);
  printf("Finding solution to A*x*y = 1, exp(-x) + exp(-y) = 1 + 1.0/A:\n");
  print_vector("Initial guess: ", x);
  steps = newton(f, x, dx, eps);
  print_vector("Solution: \t", x);
  calls = f(x,fx);
  print_vector("f(x): \t\t", fx);
  printf("Steps: %i\n", steps);
  printf("Calls: %i\n", calls-1);
  printf("\n");

  // Rosenbrock
  gsl_vector_set(x,0,0.5);
  gsl_vector_set(x,1,2);
  printf("Rosenbrock:\n");
  printf("Minimum of f(x,y) = (1-x)^2+100(y-x^2)^2:\n");
  print_vector("Initial guess: ", x);
  steps = newton(rosenbrock, x, dx, eps);
  print_vector("Solution: \t", x);
  calls = rosenbrock(x,fx);
  print_vector("grad f: \t", fx);
  printf("Steps: %i\n", steps);
  printf("Calls: %i\n", calls-1);
  printf("\n");

  // Himmelblau
  gsl_vector_set(x,0,3.5);
  gsl_vector_set(x,1,2.5);
  printf("Himmelblau:\n");
  printf("Minimum of f(x,y) = (x^2+y-11)^2+(x+y^2-7)^2:\n");
  print_vector("Initial guess: ", x);
  steps = newton(himmelblau, x, dx, eps);
  print_vector("Solution: \t", x);
  calls = himmelblau(x,fx);
  print_vector("grad f: \t", fx);
  printf("Steps: %i\n", steps);
  printf("Calls: %i\n", calls-1);
  printf("\n");

  /* --- Using explisit Jacobian --- */
  printf("--- Newton with explisit Jacobian matrix ---\n");
  printf("Using eps = %g\n", eps);
  // Linear system
  gsl_vector_set(x,0,5);
  gsl_vector_set(x,1,1);
  printf("Finding solution to A*x*y = 1, exp(-x) + exp(-y) = 1 + 1.0/A:\n");
  print_vector("Initial guess: ", x);
  steps = newton_with_jacobian(f_J, x, eps);
  print_vector("Solution: \t", x);
  calls = f_J(x,fx,NULL);
  print_vector("f(x): \t\t", fx);
  printf("Steps: %i\n", steps);
  printf("Calls: %i\n", calls-1);
  printf("\n");

  // Rosenbrock
  gsl_vector_set(x,0,0.5);
  gsl_vector_set(x,1,2);
  printf("Rosenbrock:\n");
  printf("Minimum of f(x,y) = (1-x)^2+100(y-x^2)^2:\n");
  print_vector("Initial guess: ", x);
  steps = newton_with_jacobian(rosenbrock_J, x, eps);
  print_vector("Solution: \t", x);
  calls = rosenbrock_J(x,fx,NULL);
  print_vector("grad f: \t", fx);
  printf("Steps: %i\n", steps);
  printf("Calls: %i\n", calls-1);
  printf("\n");

  // Himmelblau
  gsl_vector_set(x,0,3.5);
  gsl_vector_set(x,1,2.5);
  printf("Himmelblau:\n");
  printf("Minimum of f(x,y) = (x^2+y-11)^2+(x+y^2-7)^2:\n");
  print_vector("Initial guess: ", x);
  steps = newton_with_jacobian(himmelblau_J, x, eps);
  print_vector("Solution: \t", x);
  calls = himmelblau_J(x,fx,NULL);
  print_vector("grad f: \t", fx);
  printf("Steps: %i\n", steps);
  printf("Calls: %i\n", calls-1);
  printf("\n");

  printf("That is, in general, the two methods requres the same number of steps,\n"
         "but uses fewor function calls, because the Jacobian dosn't\n"
         "have to be calculated numerically.\n\n");

  /* --- Using GSL routines for root-finding --- */
  printf("--- gsl_multiroot_fsolver_hybrids for root-finding ---\n");
  printf("Using eps = %g\n", eps);
  // Linear system
  f(NULL,NULL); // Reset call counter
  gsl_vector_set(x,0,5);
  gsl_vector_set(x,1,1);
  printf("Finding solution to A*x*y = 1, exp(-x) + exp(-y) = 1 + 1.0/A:\n");
  print_vector("Initial guess: ", x);
  steps = gsl_root_find(f, x, eps);
  print_vector("Solution: \t", x);
  calls = f(x,fx);
  print_vector("f(x): \t\t", fx);
  printf("Steps: %i\n", steps);
  printf("Calls: %i\n", calls-1);
  printf("\n");

  // Rosenbrock
  rosenbrock(NULL,NULL); // Reset call counter
  gsl_vector_set(x,0,0.5);
  gsl_vector_set(x,1,2);
  printf("Rosenbrock:\n");
  printf("Minimum of f(x,y) = (1-x)^2+100(y-x^2)^2:\n");
  print_vector("Initial guess: ", x);
  steps = gsl_root_find(rosenbrock, x, eps);
  print_vector("Solution: \t", x);
  calls = rosenbrock(x,fx);
  print_vector("grad f: \t", fx);
  printf("Steps: %i\n", steps);
  printf("Calls: %i\n", calls-1);
  printf("\n");

  // Himmelblau
  himmelblau(NULL,NULL); // Reset call counter
  gsl_vector_set(x,0,3.5);
  gsl_vector_set(x,1,2.5);
  printf("Himmelblau:\n");
  printf("Minimum of f(x,y) = (x^2+y-11)^2+(x+y^2-7)^2:\n");
  print_vector("Initial guess: ", x);
  steps = gsl_root_find(himmelblau, x, eps);
  print_vector("Solution: \t", x);
  calls = himmelblau(x,fx);
  print_vector("grad f: \t", fx);
  printf("Steps: %i\n", steps);
  printf("Calls: %i\n", calls-1);
  printf("\n");

  printf("In conclussion the 2nd method with explicit Jacobian\n"
         "performs the best with the fewest calls to the function.\n"
         "And the GSL the worst with the most calls and steps,\n"
         "except for fewest calls using Rosenbrock");

  gsl_vector_free(fx);
  return 0;
}
