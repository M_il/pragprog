#include<assert.h>

// Using table 2 of the Interpolation chapter
// O(log n)
double linterp(int n, double* x, double* y, double z) {
  assert(n>1 && z>=x[0] && z<=x[n-1]);
  int i =0, j=n-1; /* binary search : */
  while (j-i>1) {
    int m=(i+j)/2;
    if(z>x[m])
      i=m;
    else
      j=m;
  }
  return y[i] + (y[i+1]-y[i])/(x[i+1]-x[i]) * (z-x[i]);
}

// O(n + log n) = O(n)
double linterp_integ(int n, double* x, double* y, double z) {
  double area = 0;
  int i;
  for (i = 0; x[i+1] < z; i++) {
    double deltaX = (x[i+1]-x[i]);
    area += deltaX * y[i]               // rectangle
         +  deltaX * (y[i+1] - y[i])/2; // triangle on top (note sign)
  }
  double y_end = linterp(n, x, y, z);
  area += (z-x[i]) * y[i]              // rectangle
       +  (z-x[i]) * (y_end - y[i])/2; // triangle on top (note sign)
  return area;
}
