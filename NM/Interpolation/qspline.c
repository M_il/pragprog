#import "qspline.h"
#include <stdlib.h>
#include <assert.h>

/* allocates and builds the quadratic spline */
qspline* qspline_alloc(int n, double *x, double *y) { //builds qspline
  qspline* s = (qspline*)malloc(sizeof(qspline)); //spline

  s->b = (double*)malloc((n-1)*sizeof(double));
  s->c = (double*)malloc((n-1)*sizeof(double));
  s->x = (double*)malloc(n*sizeof(double));
  s->y = (double*)malloc(n*sizeof(double));
  s->n = n;

  for (int i=0;i<n; i++) {              // Copy coordinates
    s->x[i]=x[i];
    s->y[i]=y[i];
  }

  // h_i is ∆x_i, p the slopes
  int i; double p[n-1], h[n-1];         //VLA from C99
  for (i=0;i<n-1;i++){
    h[i]=x[i+1]-x[i];
    p[i]=(y[i+1]-y[i])/h[i];
  }

  s->c[0]=0;                            //recursion up:
  for (i=0;i<n-2;i++) {
    s->c[i+1]=(p[i+1]-p[i]-s->c[i]*h[i])/h[i+1];
  }

  // Mean value by division by 2
  s->c[n-2]/=2;                         //recursion down:
  for (i=n-3;i>=0;i--){
    s->c[i]=(p[i+1]-p[i]-s->c[i+1]*h[i+1])/h[i];
  }

  for (i=0;i<n-1;i++){
    s->b[i]=p[i]-s->c[i]*h[i];
  }
  return s;
}

/* evaluates the prebuilt spline at point z */
double qspline_evaluate(qspline *s, double z){
  assert(z>=s->x[0] && z<=s->x[s->n-1]);
  int i=0, j=s->n-1; //binary search:
  while(j-i>1) {
    int m=(i+j)/2;
    if(z>s->x[m])
      i=m;
    else
      j=m;
  }
  double h=z-s->x[i];
  return s->y[i] + h*(s->b[i] + h*s->c[i]); //inerpolating polynomial
}

/* evaluates the derivative of the prebuilt spline at point z */
/* y = yi + b*h + c*h^2, h = z - xi
   dy/dz = df/dh * dh/dz = df/dh = b + 2*c*h
*/
double qspline_derivative(qspline *s, double z) {
  assert(z>=s->x[0] && z<=s->x[s->n-1]);
  int i=0, j=s->n-1; //binary search:
  while(j-i>1) {
    int m=(i+j)/2;
    if(z>s->x[m])
      i=m;
    else
      j=m;
  }
  double h=z-s->x[i];
  return s->b[i] + 2*s->c[i]*h; //inerpolating polynomial derivative
}

/* evaluates the integral of the prebuilt spline from x[0] to z */
/* Si = yi + b*h + c*h^2 and h = z - xi => dh = dz
   ∫Si(z)dz from xa to xb = ∫Si(h)dh from xa - xi to xb - xi with (xi <= xa < xb <= x_i+1)
      = [yi*h + 1/2 * b*h^2 + 1/3 * c*h^3] from xa - xi to xb - xi

   First: for all i in xi < z
    add integrals between xi and x_i+1
    h_start = 0
    h_end   = x_i+1 - xi
   Then: for i in xi < z < x_i+1
    add interal betwen xi and z
*/
double qspline_integral(qspline *s, double z) {
  assert(z>=s->x[0] && z<=s->x[s->n-1]);
  double *x = s->x, *y = s->y, *b = s->b, *c = s->c;
  double area = 0;
  int i;
  for (i = 0; x[i+1] < z; i++) {
    double h_end = x[i+1] - x[i];
    area += (((c[i]/3*h_end + b[i]/2)*h_end + y[i])*h_end); // equvalent to y[i] * h_end + b/2 * pow(h_end,2) + c/3  * pow(h_end,3);
  }
  double h_end = z - x[i];
  area += (((c[i]/3*h_end + b[i]/2)*h_end + y[i])*h_end);
  return area;
}

/* free memory allocated in qspline_alloc */
void qspline_free(qspline *s) {
  free(s->b);
  free(s->c);
  free(s->x);
  free(s->y);
  free(s);
}
