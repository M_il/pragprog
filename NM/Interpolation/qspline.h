#ifndef QSPLINE_H
#define QSPLINE_H
typedef struct {int n; double *x, *y, *b, *c;} qspline;
qspline* qspline_alloc(int n, double *x, double *y);    /* allocates and builds the quadratic spline */
double qspline_evaluate(qspline *s, double z);   /* evaluates the prebuilt spline at point z */
double qspline_derivative(qspline *s, double z); /* evaluates the derivative of the prebuilt spline at point z */
double qspline_integral(qspline *s, double z);   /* evaluates the integral of the prebuilt spline from x[0] to z */
void qspline_free(qspline *s);                   /* free memory allocated in qspline_alloc */
#endif
