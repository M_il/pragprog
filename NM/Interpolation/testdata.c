#include <stdio.h>
#include <math.h>
#include "linterp.h"
#include "qspline.h"
#include "cspline.h"
#include <gsl/gsl_spline.h>

int main(int argc, char const *argv[]) {
  int samples = 31;
  double x_max = 6*M_PI;

  // Sample data
  printf("# Samples data\n");
  double x[samples], y[samples];
  for (int i = 0; i < samples; i++) {
    x[i] = ((double)i)/samples * x_max;
    y[i] = cos(x[i]);
    printf("%g %g\n", x[i], y[i]);
  }

  // Linear spline
  double linterp_step = (x_max/samples)/10;
  {
    printf("\n\n# Linear interpolation"); // Separate the datasets
    for (double xi = x[0]; xi <= x[samples-1]; xi+=linterp_step) {
      printf("%g %g\n", xi, linterp(samples, x, y, xi));
    }

    printf("\n\n# Linear integration");
    for (double xi = x[0]; xi <= x[samples-1]; xi+=linterp_step) {
      printf("%g %g\n", xi, linterp_integ(samples, x, y, xi));
    }
  }

  // Quadratic spline
  {
    printf("\n\n# Quadratic interpolation");
    qspline* s = qspline_alloc(samples, x, y);
    for (double xi = x[0]; xi <= x[samples-1]; xi+=linterp_step) {
      printf("%g %g\n", xi, qspline_evaluate(s, xi));
    }

    printf("\n\n# Quadratic derivative");
    for (double xi = x[0]; xi <= x[samples-1]; xi+=linterp_step) {
      printf("%g %g\n", xi, qspline_derivative(s, xi));
    }

    printf("\n\n# Quadratic integral");
    for (double xi = x[0]; xi <= x[samples-1]; xi+=linterp_step) {
      printf("%g %g\n", xi, qspline_integral(s, xi));
    }
    qspline_free(s);
  }

  // Cubic spline
  {
    printf("\n\n# Cubic interpolation");
    cubic_spline* s = cubic_spline_alloc(samples, x, y);
    for (double xi = x[0]; xi <= x[samples-1]; xi+=linterp_step) {
      printf("%g %g\n", xi, cubic_spline_eval(s, xi));
    }

    printf("\n\n# Cubic derivative");
    for (double xi = x[0]; xi <= x[samples-1]; xi+=linterp_step) {
      printf("%g %g\n", xi, cubic_spline_derivative(s, xi));
    }

    printf("\n\n# Cubic integral");
    for (double xi = x[0]; xi <= x[samples-1]; xi+=linterp_step) {
      printf("%g %g\n", xi, cubic_spline_integral(s, xi));
    }
    cubic_spline_free(s);
  }

  // GSL cubic spline comparason
  printf("\n\n# GSL cubic spline comparason");
  gsl_interp_accel *acc = gsl_interp_accel_alloc();
  gsl_spline *spline = gsl_spline_alloc(gsl_interp_cspline, samples);

  gsl_spline_init(spline, x, y, samples);

  for (double xi = x[0]; xi <= x[samples-1]; xi+=linterp_step) {
    double yi = gsl_spline_eval(spline, xi, acc);
    printf("%g %g\n", xi, yi);
  }
  gsl_spline_free(spline);
  gsl_interp_accel_free(acc);

  return 0;
}
