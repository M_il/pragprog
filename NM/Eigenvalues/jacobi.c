#include "jacobi.h"

// Exercise A
int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V) {
  /* Jacobi diagonalization; upper triangle of A is destroyed;
     e and V accumulate eigenvalues and eigenvectors */
  // Returns sweeps: number of itterations over all off-diagonal elements
  bool changed;
  int sweeps=0, n=A->size1;
  for(int i=0;i<n;i++) {
    gsl_vector_set(e,i,gsl_matrix_get(A,i,i));
  }
  gsl_matrix_set_identity(V);

  do {
    changed=0; sweeps++; int p,q;
    for (p=0;p<n;p++) {
      for (q=p+1;q<n;q++) {
        double App=gsl_vector_get(e,p);
        double Aqq=gsl_vector_get(e,q);
        double Apq=gsl_matrix_get(A,p,q);
        double phi=0.5*atan2(2*Apq,Aqq-App);
        double c = cos(phi), s = sin(phi);
        double App1=c*c*App-2*s*c*Apq+s*s*Aqq;
        double Aqq1=s*s*App+2*s*c*Apq+c*c*Aqq;

        // If any of the diagonal elements have changed,
        // then we have to apply the rest of the equations as well
        if (App1!=App || Aqq1!=Aqq) {
          changed=true;
          gsl_vector_set(e,p,App1);
          gsl_vector_set(e,q,Aqq1);
          gsl_matrix_set(A,p,q,0.0);  // Implied from choice of phi: eq. 10

          // 2'nd and 3'rd equations of eq. 9
          for (int i=0;i<p;i++) {
            double Aip=gsl_matrix_get(A,i,p);
            double Aiq=gsl_matrix_get(A,i,q);
            gsl_matrix_set(A,i,p,c*Aip-s*Aiq);
            gsl_matrix_set(A,i,q,c*Aiq+s*Aip);
          }
          for (int i=p+1;i<q;i++) {
            double Api=gsl_matrix_get(A,p,i);
            double Aiq=gsl_matrix_get(A,i,q);
            gsl_matrix_set(A,p,i,c*Api-s*Aiq);
            gsl_matrix_set(A,i,q,c*Aiq+s*Api);
          }
          for (int i=q+1;i<n;i++) {
            double Api=gsl_matrix_get(A,p,i);
            double Aqi=gsl_matrix_get(A,q,i);
            gsl_matrix_set(A,p,i,c*Api-s*Aqi);
            gsl_matrix_set(A,q,i,c*Aqi+s*Api);
          }

          // Eq. 11
          for (int i=0;i<n;i++) {
            double vip=gsl_matrix_get(V,i,p);
            double viq=gsl_matrix_get(V,i,q);
            gsl_matrix_set(V,i,p,c*vip-s*viq);
            gsl_matrix_set(V,i,q,c*viq+s*vip);
          }
        }
      }
    }
  } while (changed == true);

  return sweeps;
}

// Exersice B
int jacobi_n(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int n) {
  /* Jacobi diagonalization; upper triangle of A is destroyed;
     e and V accumulate m eigenvalues and eigenvectors, e and void
     must still contain the rows in A number of entries/columns. */
  // Returns sweeps: number of itterations over all off-diagonal elements
  bool changed;
  int sweeps=0, m=A->size1;
  for(int i=0;i<m;i++) {
    gsl_vector_set(e,i,gsl_matrix_get(A,i,i));
  }
  gsl_matrix_set_identity(V);

  // Now calculate egienvalues eigenvectors one at a time
  int p,q;
  for (p=0;p<n;p++) {
    do {
      changed=0; sweeps++;
      for (q=p+1;q<m;q++) {
        double App=gsl_vector_get(e,p);
        double Aqq=gsl_vector_get(e,q);
        double Apq=gsl_matrix_get(A,p,q);
        double phi=0.5*atan2(2*Apq,Aqq-App);
        double c = cos(phi), s = sin(phi);
        double App1=c*c*App-2*s*c*Apq+s*s*Aqq;
        double Aqq1=s*s*App+2*s*c*Apq+c*c*Aqq;

        // If any of the diagonal elements have changed,
        // then we have to apply the rest of the equations as well
        if (App1!=App || Aqq1!=Aqq) {
          changed=true;
          gsl_vector_set(e,p,App1);
          gsl_vector_set(e,q,Aqq1);
          gsl_matrix_set(A,p,q,0.0);  // Implied from choice of phi: eq. 10

          // 2'nd and 3'rd equations of eq. 9
          /* This first first loop is not needed anymore as:
             Assuming that the first p-1 eigenvalues are calculated,
             then their respecitve rows and columns are zeroed:
               Aij = Aji = 0 ∀i ∀j<p  (1)
             Then for the p'th eigenvalue, the subsequent Jacobi rotations will
             affect Aab via 2'nd and 3-rd eq. of of eq. 9 in the follewing way
               Api1 = Aip1 = cApi - sAqi ∀i<p
               Aqi1 = Aiq1 = sApi + cAqi ∀i<p
             As Api = Aqi = 0 according to (1), there is no change for i<p.
          */
          // for (int i=0;i<p;i++) { ... }
          for (int i=p+1;i<q;i++) {
            double Api=gsl_matrix_get(A,p,i);
            double Aiq=gsl_matrix_get(A,i,q);
            gsl_matrix_set(A,p,i,c*Api-s*Aiq);
            gsl_matrix_set(A,i,q,c*Aiq+s*Api);
          }
          for (int i=q+1;i<m;i++) {
            double Api=gsl_matrix_get(A,p,i);
            double Aqi=gsl_matrix_get(A,q,i);
            gsl_matrix_set(A,p,i,c*Api-s*Aqi);
            gsl_matrix_set(A,q,i,c*Aqi+s*Api);
          }

          // Eq. 11
          for (int i=0;i<m;i++) {
            double vip=gsl_matrix_get(V,i,p);
            double viq=gsl_matrix_get(V,i,q);
            gsl_matrix_set(V,i,p,c*vip-s*viq);
            gsl_matrix_set(V,i,q,c*viq+s*vip);
          }
        }
      }
    } while (changed == true);
  }

  return sweeps;
}
