#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include "jacobi.h"
#include "matrix_print.h"
#define RND ((double)rand()/RAND_MAX)
#define max_print 11

int main(int argc, char** argv) {
  // matrix dim is 5 if no n is privded
  int n = (argc>1) ? atoi(argv[1]) : 5;

  // Generate a random matrix, A, and a copy hereof, B
  gsl_matrix *A = gsl_matrix_alloc(n,n);
  gsl_matrix *B = gsl_matrix_alloc(n,n);
  for(int i=0;i<n;i++) {
    for(int j=i;j<n;j++) {
      double x = RND;
      gsl_matrix_set(A,i,j,x);
      gsl_matrix_set(A,j,i,x);
    }
  }
  gsl_matrix_memcpy(B,A);

  // Find egien-values and -vectors using jacobi-diag. using cyclic sweeps
  gsl_matrix *V = gsl_matrix_alloc(n,n);
  gsl_vector *e = gsl_vector_alloc(n);
  // Calculate half of the eigenvalues
  int sweeps = jacobi_n(A,e,V,(int)(0.5*n));
  printf("n=%i, sweeps=%i\n",n,sweeps);

  // Print the rusults if the matrices aren't too big
  if (n<max_print) {
    fprintf(stderr,"\na random symmetric matrix A: \n");
    matrix_print(B);
    fprintf(stderr,"\nthe result of Jacobi diagonalization of half of matrix: \n");
    fprintf(stderr,"sweeps\t = %d\n",sweeps);
    fprintf(stderr,"eigenvalues:\n");
    vector_print(e);
    gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,B,V,0,A);	// (B*V)
    gsl_blas_dgemm(CblasTrans  ,CblasNoTrans,1,V,A,0,B);	// V^T*(B*V)
    fprintf(stderr, "check: V^T*A*V should be diagonal with above eigenvalues:\n");
    matrix_print(B);
  }

  gsl_matrix_free(A);gsl_matrix_free(B);gsl_matrix_free(V);
  gsl_vector_free(e);
  return 0;
}
