#import "matrix_print.h"
#define FMT "%7.3f"

void matrix_print(gsl_matrix *A){
	for (int r=0;r<A->size1;r++) {
		for (int c=0;c<A->size2;c++) {
      fprintf(stderr,FMT,gsl_matrix_get(A,r,c));
    }
		fprintf(stderr,"\n");
  }
}

void vector_print(gsl_vector *v){
	for(int i=0;i<v->size;i++) {
    fprintf(stderr,FMT,gsl_vector_get(v,i));
  }
	fprintf(stderr,"\n");
}
