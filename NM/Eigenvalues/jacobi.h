#ifndef JACOBI_H
#define JACOBI_H
#include <math.h>
#include <stdbool.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

int jacobi(gsl_matrix *A, gsl_vector *e, gsl_matrix *V);
int jacobi_n(gsl_matrix *A, gsl_vector *e, gsl_matrix *V, int n);
#endif
