#ifndef MATRIX_PRINT_H
#define MATRIX_PRINT_H
#include <gsl/gsl_matrix.h>

void matrix_print(gsl_matrix *A);
void vector_print(gsl_vector *v);
#endif
