#include "qr.h"

// Performs in-place modified Gram-Schmidt orthogonalization
// of an n×m (n≥m) matrix A:
// A turns into Q and the square m×m matrix R is computed.
void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R) {
  int m = A->size2;
  for(int i=0;i<m;i++){
    gsl_vector_view ai = gsl_matrix_column(A,i);
    double Rii = gsl_blas_dnrm2(&ai.vector);        // Rii = ||ai||
    gsl_matrix_set(R,i,i,Rii);                      //
    gsl_vector_scale(&ai.vector,1/Rii);             // qi = ai/Rii -> ai
    gsl_vector_view qi = ai;                        //

    for(int j=i+1;j<m;j++){
      gsl_vector_view aj = gsl_matrix_column(A,j);
      double Rij=0; gsl_blas_ddot(&qi.vector,&aj.vector,&Rij);  // Rij = <qi|aj>
      gsl_matrix_set(R,i,j,Rij);                                //
      gsl_matrix_set(R,j,i,0);                                  // Rji = 0
      gsl_blas_daxpy(-Rij,&qi.vector,&aj.vector);               // -Rij*qi + aj -> aj
    }
  }
}


// Backsubsitution
void qr_bak(const gsl_matrix *R, gsl_vector *x) {
  int m=R->size1;
  for(int i=m-1;i>=0;i--){
    double s=0;
    for(int k=i+1;k<m;k++)
      s+=gsl_matrix_get(R,i,k)*gsl_vector_get(x,k);
    gsl_vector_set(x,i,(gsl_vector_get(x,i)-s)/gsl_matrix_get(R,i,i));
  }
}

// given the matrices Q and R from qr_gs_decomp—solves the equation QRx=b by applying QT to the vector b (saving the result in x) and then performing in-place back-substitution on x.
void qr_gs_solve(const gsl_matrix* Q,
                 const gsl_matrix* R,
                 const gsl_vector* b,
                 gsl_vector* x) {
  gsl_blas_dgemv(CblasTrans,1.0,Q,b,0.0,x); // x = 1*Q^T*b + 0*x
  qr_bak(R,x); // backsustitution
}

// given the matrices Q and R from qr_gs_decomp
// calculates the inverse of the matrix A into the matrix B
void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B) {
  int n=R->size1;
  gsl_vector *b = gsl_vector_calloc(n);
  gsl_vector *x = gsl_vector_calloc(n);
  for(int i=0;i<n;i++){
    gsl_vector_set(b,i,1.0);
    qr_gs_solve(Q,R,b,x);
    gsl_vector_set(b,i,0.0);
    gsl_matrix_set_col(B,i,x);
  }
  gsl_vector_free(b);
  gsl_vector_free(x);
}
